/*
 Navicat Premium Data Transfer

 Source Server         : indojayapvc
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : indojayapvc.com:3306
 Source Schema         : indojay9_fo

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 16/03/2022 23:19:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for category_product
-- ----------------------------
DROP TABLE IF EXISTS `category_product`;
CREATE TABLE `category_product`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `link` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `hilight_jasa` tinyint(1) NULL DEFAULT NULL,
  `keyword` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `position` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category_product
-- ----------------------------
INSERT INTO `category_product` VALUES (1, 'Atap Bajaringan', 'atapbajaringan', 1, 1, NULL, 1, NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `category_product` VALUES (2, 'Plafon Gypsum', 'plafongypsum', 1, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (3, 'List Gypsum', 'listgypsum', 0, 0, NULL, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (4, 'Partisi Gypsum', 'partisigypsum', 0, 0, NULL, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (5, 'Moulding', 'moulding', 0, 0, NULL, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (6, 'Plafon PVC', 'plafonpvc', 1, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (7, 'Partisi PVC', 'partisipvc', 0, 0, NULL, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (8, 'Cutting Board PVC', 'cuttingboardpvc', 0, 0, NULL, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (9, 'Lis PVC', 'lispvc', 0, 0, NULL, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (10, 'Ornamen PVC  ', 'ornamenpvc', 0, 0, NULL, 10, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint unsigned NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int unsigned NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int unsigned NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (4, 'Produk', 'Produk', 'fa fa-arrow-circle-right', 2, 'product', 'DTA', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (5, 'Groups', 'Groups', 'fa fa-arrow-circle-right', 0, 'groups', 'DTA', NULL, NULL, NULL, NULL, '2022-03-09 21:04:34');
INSERT INTO `menu` VALUES (6, 'Category', 'Category', 'fa fa-arrow-circle-right', 1, 'categoryproduct', 'DTA', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (7, 'Project', 'Project', 'fa fa-arrow-circle-right', 3, 'project', 'DTA', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (8, 'Setting', 'Setting', 'fa fa-arrow-circle-right', 7, 'setting', 'DTA', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (9, 'Slider', 'Slider', 'fa fa-arrow-circle-right', 4, 'slider', 'DTA', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (10, 'Jasa', 'Jasa', 'fa fa-arrow-circle-right', 5, 'service', 'DTA', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (11, 'Top Product', 'Top Product', 'fa fa-arrow-circle-right', 6, 'topproduct', 'DTA', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menu_action
-- ----------------------------
DROP TABLE IF EXISTS `menu_action`;
CREATE TABLE `menu_action`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `menu_id` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_action
-- ----------------------------
INSERT INTO `menu_action` VALUES (1, 'add', 'fa fa-arrow-circle-right', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_action` VALUES (2, 'edit', 'fa fa-arrow-circle-right', 2, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_action` VALUES (3, 'delete', 'fa fa-arrow-circle-right', 3, 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menu_category
-- ----------------------------
DROP TABLE IF EXISTS `menu_category`;
CREATE TABLE `menu_category`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `kode` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_category
-- ----------------------------
INSERT INTO `menu_category` VALUES (2, 'DTA', 'Menu', 'Menu', 'fas fa-table', 2, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `group_id` int(0) NOT NULL,
  `menu_id` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3962 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (3949, 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3950, 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3951, 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3952, 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3953, 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3954, 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3955, 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3956, 2, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3957, 2, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3958, 2, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3959, 2, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3960, 2, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3961, 2, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission` VALUES (3962, 2, 11, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for permission_action
-- ----------------------------
DROP TABLE IF EXISTS `permission_action`;
CREATE TABLE `permission_action`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `permission_id` int(0) NOT NULL,
  `menu_action_id` int(0) NULL DEFAULT NULL,
  `group_id` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission_action
-- ----------------------------
INSERT INTO `permission_action` VALUES (38, 1, 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission_action` VALUES (39, 1, 2, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission_action` VALUES (40, 1, 3, 3, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name_product` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `category` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 139 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (23, 'CB-101', '8', 'CB-101.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (24, 'CB-102', '8', 'CB-102.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (25, 'CB-103', '8', 'CB-103.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (26, 'CB-104', '8', 'CB-104.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (27, 'CB-105', '8', 'CB-105.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (28, 'CB-106', '8', 'CB-106.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (29, 'CB-107', '8', 'CB-107.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (30, 'CB-108', '8', 'CB-108.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (31, 'CB-109', '8', 'CB-109.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (32, 'CB-110', '8', 'CB-110.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (33, 'CB-111', '8', 'CB-111.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (34, 'CB-112', '8', 'CB-112.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (35, 'CB-113', '8', 'CB-113.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (36, 'CB-114', '8', 'CB-114.jpg', 1, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (37, 'CB-115', '8', 'CB-115.jpg', 1, 15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (38, 'CB-116', '8', 'CB-116.jpg', 1, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (39, 'CB-117', '8', 'CB-117.jpg', 1, 17, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (40, 'CB-118', '8', 'CB-118.jpg', 1, 18, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (41, 'CB-119', '8', 'CB-119.jpg', 1, 19, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (42, 'CB-120', '8', 'CB-120.jpg', 1, 20, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (48, 'OD601', '5', 'OD601.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (49, 'OD602', '5', 'OD602.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (50, 'OD603', '5', 'OD603.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (51, 'OD604', '5', 'OD604.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (52, 'OD605', '5', 'OD605.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (53, 'OD606', '5', 'OD606.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (54, 'OD607', '5', 'OD607.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (55, 'OD608', '5', 'OD608.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (56, 'OD609', '5', 'OD609.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (57, 'C 505 S', '3', 'C 505 S.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (58, 'C 506 S', '3', 'C 506 S.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (59, 'C 509 K', '3', 'C 509 K.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (60, 'C 510 K', '3', 'C 510 K.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (61, 'C 511 K', '3', 'C 511 K.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (62, 'C 512 K', '3', 'C 512 K.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (63, 'IJ-301', '6', 'IJ-301.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (64, 'IJ-302', '6', 'IJ-302.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (65, 'IJ-303', '6', 'IJ-303.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (66, 'IJ-304', '6', 'IJ-304.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (67, 'IJ-305', '6', 'IJ-305.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (68, 'IJ-306', '6', 'IJ-306.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (69, 'IJ-307', '6', 'IJ-307.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (70, 'IJ-308', '6', 'IJ-308.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (71, 'IJ-309', '6', 'IJ-309.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (72, 'IJ-310', '6', 'IJ-310.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (73, 'IJ-311', '6', 'IJ-311.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (74, 'IJ-312', '6', 'IJ-312.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (75, 'IJ-313', '6', 'IJ-313.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (76, 'IJ-314', '6', 'IJ-314.jpg', 1, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (77, 'IJ-315', '6', 'IJ-315.jpg', 1, 15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (78, 'IJ-316', '6', 'IJ-316.jpg', 1, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (79, 'IJ-317', '6', 'IJ-317.jpg', 1, 17, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (80, 'IJ-318', '6', 'IJ-318.jpg', 1, 18, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (81, 'IJ-319', '6', 'IJ-319.jpg', 1, 19, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (82, 'IJ-320', '6', 'IJ-320.jpg', 1, 20, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (83, 'IJ-321', '6', 'IJ-321.jpg', 1, 21, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (84, 'IJ-322', '6', 'IJ-322.jpg', 1, 22, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (85, 'IJ-323', '6', 'IJ-323.jpg', 1, 23, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (86, 'IJ-324', '6', 'IJ-324.jpg', 1, 24, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (87, 'IJ-325', '6', 'IJ-325.jpg', 1, 25, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (88, 'IJ-326', '6', 'IJ-326.jpg', 1, 26, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (89, 'IJ-327', '6', 'IJ-327.jpg', 1, 27, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (90, 'IJ-328', '6', 'IJ-328.jpg', 1, 28, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (91, 'IJ-329', '6', 'IJ-329.jpg', 1, 29, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (92, 'IJ-330', '6', 'IJ-330.jpg', 1, 30, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (93, 'LP-901', '9', 'LP-901.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (94, 'LP-902', '9', 'LP-902.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (95, 'LP-903', '9', 'LP-903.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (96, 'LP-904', '9', 'LP-904.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (97, 'LP-905', '9', 'LP-905.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (98, 'LP-906', '9', 'LP-906.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (99, 'LP-907', '9', 'LP-907.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (100, 'LP-908', '9', 'LP-908.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (101, 'LP-909', '9', 'LP-909.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (102, 'LP-910', '9', 'LP-910.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (103, 'LP-911', '9', 'LP-911.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (104, 'LP-912', '9', 'LP-912.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (105, 'LP-913', '9', 'LP-913.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (106, 'LP-914', '9', 'LP-914.jpg', 1, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (107, 'LP-915', '9', 'LP-915.jpg', 1, 15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (108, 'LP-916', '9', 'LP-916.jpg', 1, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (109, 'OR-701', '10', 'OR-701.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (110, 'OR-702', '10', 'OR-702.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (111, 'OR-703', '10', 'OR-703.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (112, 'OR-704', '10', 'OR-704.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (113, 'OR-705', '10', 'OR-705.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (114, 'OR-706', '10', 'OR-706.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (115, 'OR-707', '10', 'OR-707.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (116, 'OR-708', '10', 'OR-708.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (117, 'OR-709', '10', 'OR-709.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (118, 'OR-710', '10', 'OR-710.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (120, 'C 501 B', '3', 'C 501 B.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (121, 'C 502 B', '3', 'C 502 B.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (122, 'C 503 B', '3', 'C 503 B.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (123, 'C 504 B', '3', 'C 504 B.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (124, 'C 507 S', '3', 'C 507 S.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (125, 'C 508 S', '3', 'C 508 S.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (126, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54 (6).jpeg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (127, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54.jpeg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (128, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.10.23 (1).jpeg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (129, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.10.23 (2).jpeg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (130, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.10.23.jpeg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (131, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.10.31.jpeg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (132, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.10.35 (1).jpeg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (133, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.10.35.jpeg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (134, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54 (1).jpeg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (135, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54 (2).jpeg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (136, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54 (3).jpeg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (137, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54 (4).jpeg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (138, NULL, '4', 'WhatsApp Image 2022-03-01 at 10.15.54 (5).jpeg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (139, NULL, '2', 'WhatsApp Image 2022-03-01 at 10.15.54.jpeg', 1, 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name_project` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `company` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES (14, 'Four Points Makassar', 'Jl. Andi Djemma No.130, Banta-Bantaeng, Kec. Rappocini, Kota Makassar', 'PT. IMB', 'fourpoint.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (15, 'Bank BCA KCU Panakkukang', 'Jl. Boulevard No.5, Pisang Utara, Kec. Ujung Pandang, Kota Makassar', 'PT. Pulau Intan', 'BCA.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (16, 'Bank Indonesia Cabang Sul-Sel', 'Jl. Jend. Sudirman No.3, Pisang Utara, Kec. Ujung Pandang, Kota Makassar', 'PT. Nindya Karya', 'BI.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (17, 'RSU KOLAKA', 'Jl. Mekongga Indah, Tahoa, Kec. Kolaka, Kabupaten Kolaka', 'PT. Brantas Abibraya', 'RSKolaka.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (19, 'RS Tadjuddin', NULL, 'PT. PP Urban', 'RSTadjuddin.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (20, 'Bandara EL Tari', NULL, 'PT. Nindya Karya', 'ELTari.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (21, 'Poltekpar Lombok', NULL, 'PT. Nindya Karya', 'Poltekpar.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (22, 'Nipah Mall', NULL, 'PT. PP Persero', 'NipahMall.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (23, 'Bandara Samratulangi Manado', NULL, 'PT. Adhi Karya', 'Samratulangi.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (24, 'Gudang Indofood', NULL, 'PT. Total Persada Indonesia', 'Indofood.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `service_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `category` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES (1, 'Pemasangan Atap', '1', '6a0f1150-e551-4543-8bf3-ee4664a72cac.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (2, 'Pemasangan Atap', '1', '7ce6133e-7ae9-4412-b02b-f1df7be06c62.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (3, 'Pemasangan Atap', '1', '8d5a11ea-9434-4624-b8fa-ca275474d955.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (4, 'Pemasangan Atap', '1', '50f5dbce-945a-4f83-85b9-8610f39e462c.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (5, 'Pemasangan Atap', '1', '54c9be58-6794-4bb6-8d00-b748cf9f06a0.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (6, 'Pemasangan Atap', '1', '12043729-5e1a-433f-9382-d41c188bea7c.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (7, 'Pemasangan Atap', '1', '76303553-2cde-40c8-87a0-1f0d6e5f2dba.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (8, 'Pemasangan Atap', '1', 'a8bea7a0-ca49-4171-ba6c-df16366adac7.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (9, 'Pemasangan Atap', '1', 'a8bea7a0-ca49-4171-ba6c-df16366adac6.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (10, 'Pemasangan Atap', '1', 'a44fbd3b-3dc4-4bc1-b3fa-00b053c89874.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (11, 'Pemasangan Atap', '1', 'a072d171-01c2-4a08-b43d-afeebec19a3d.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (12, 'Pemasangan Atap', '1', '0ebcd477-8855-430f-9e21-bb3020e30913.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (13, 'Pemasangan Atap', '1', '3fc8b726-cdbe-467b-8229-20c09e38d443.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (14, 'Plafon PVC', '6', 'ab0556c4-9e29-43ef-a817-0d18c026851c.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (15, 'Plafon PVC', '6', 'b33e05d6-10ad-4a29-b3e1-1d99170e2ae3.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (16, 'Plafon PVC', '6', 'fcfc2523-931a-46b7-bce3-e465fd77b42f.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (17, 'Plafon PVC', '6', 'e8f433c2-2c4a-4b21-99a9-4c22c8cfe39e.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (18, 'Plafon PVC', '6', '4c15ea88-71cd-4357-afd7-5abab2711910.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (19, 'Plafon PVC', '6', '6fcaeaeb-52ba-4078-8046-07c5bab3d3bc.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (20, 'Plafon PVC', '6', '8a0fbd14-6ea2-4817-b66d-9cf7e2de8c8a.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (21, 'Plafon PVC', '6', '8a859ef6-b435-4da8-be39-5bafe5da3a7f.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (22, 'Plafon PVC', '6', '13f922f6-4b0f-4f50-8478-8e968c570408.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (23, 'Plafon PVC', '6', '41f5e6a4-f8c2-4e65-bd8d-aa5230b767ba.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (24, 'Plafon PVC', '6', '44a004da-8e6f-44c7-8471-46bbc9c41a04.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (25, 'Plafon PVC', '6', '054fca00-ab23-4af5-a484-de90702a0be0.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (26, 'Plafon PVC', '6', '7899da59-b0e1-40b5-9927-9104e41c8776.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (27, 'Plafon Gypsum', '2', '04d7b6cf-0c7c-4b0f-80e4-74ecf7eff26d.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (28, 'Plafon Gypsum', '2', '6fcaeaeb-52ba-4078-8046-07c5bab3d3bc.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (29, 'Plafon Gypsum', '2', '7a00d585-33af-4c61-b535-63d5a56a1497.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (30, 'Plafon Gypsum', '2', '8adb1886-5330-4f11-9fbf-5fa8510fc320.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (31, 'Plafon Gypsum', '2', '49e15a66-e88a-4c69-a9ac-19252f2d7fc0.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (32, 'Plafon Gypsum', '2', '55d5af7b-0ce1-4a6d-919a-2043b9692200.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (33, 'Plafon Gypsum', '2', '047469ae-1125-4668-8c3f-2a57cf720543.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (34, 'Plafon Gypsum', '2', 'a376ef09-784a-4fa3-ac28-32f977ef8a2a.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `service` VALUES (35, 'Plafon Gypsum', '2', 'b0cefc5b-9104-46fc-8700-294554048697.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `contac` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field1` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field2` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field3` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES (1, '6282344444422', NULL, NULL, NULL, NULL, NULL, '2021-06-23 01:57:32', 1, NULL);

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `link` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` enum('Desktop','Mobile','') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES (14, 'WEB.png', 'moulding', 1, 2, 'pesan sekarang', 'Desktop', NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `slider` VALUES (15, 'WEB2.jpg', 'cuttingboardpvc', 1, 3, 'pesan sekarang', 'Desktop', NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `slider` VALUES (16, 'SliderHome.jpg', '#', 1, 1, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (17, 'SliderHomeMobile.jpg', '#', 1, 1, 'pesan sekarang', 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (18, 'KTG1.jpg', 'plafonpvc', 1, 4, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (19, 'KTG2.jpg', 'plafonpvc', 1, 5, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (20, 'KTG3.jpg', 'plafonpvc', 1, 6, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (21, 'KTG4.jpg', 'plafonpvc', 1, 7, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (22, 'ornamenpvc.jpg', 'ornamenpvc', 1, 8, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (23, 'slide%20or2.jpg', 'ornamenpvc', 1, 2, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (24, 'slide_cb1.jpg', 'cuttingboardpvc', 1, 1, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (25, 'slide%20cb2.jpg', 'cuttingboardpvc', 1, 2, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (26, 'slide%20cb3.jpg', 'cuttingboardpvc', 1, 3, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (27, 'slide%20cb4.jpg', 'cuttingboardpvc', 1, 4, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (28, 'slide%20cb5.jpg', 'cuttingboardpvc', 1, 5, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (29, 'slide%20od1.jpg', 'moulding', 1, 1, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (30, 'slide%20od2.jpg', 'moulding', 1, 2, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (31, 'ornamenpvc.jpg', 'ornamenpvc', 1, 1, NULL, 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (32, 'D%20Plafon%20PVC.jpg', 'lispvc', 1, 1, NULL, 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (33, 'D%20Plafon%20PVC2.jpg', 'lispvc', 1, 2, NULL, 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (34, 'slide%20ij1.jpg', 'plafonpvc', 1, 1, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (35, 'slide%20ij2.jpg', 'plafonpvc', 1, 2, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (36, 'slide%20ij3.jpg', 'plafonpvc', 1, 3, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (37, 'slide%20ij4.jpg', 'plafonpvc', 1, 4, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (38, 'slide%20lp1.jpg', 'lispvc', 1, 1, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (39, 'slide%20lp2.jpg', 'lispvc', 1, 2, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (40, 'slide%20lp3.jpg', 'lispvc', 1, 3, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (41, 'slide%20lp4.jpg', 'lispvc', 1, 4, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (42, 'slide%20or%203.jpg', 'ornamenpvc', 1, 3, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (43, 'slide%20or1.jpg', 'ornamenpvc', 1, 1, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (44, 'slidepgD1.jpg', 'listgypsum', 1, 1, NULL, 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (45, 'slidepg2.jpg', 'listgypsum', 1, 2, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (46, 'slidepg1.jpg', 'listgypsum', 1, 1, NULL, 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (47, 'slidepgD2.jpg', 'listgypsum', 1, 2, NULL, 'Desktop', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for top_product
-- ----------------------------
DROP TABLE IF EXISTS `top_product`;
CREATE TABLE `top_product`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name_product` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `category` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of top_product
-- ----------------------------
INSERT INTO `top_product` VALUES (1, 'C 501 B', '3', 'C 501 B.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (2, 'C 505 S', '3', 'C 505 S.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (3, 'C 506 S', '3', 'C 506 S.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (4, 'OD601', '5', 'OD601.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (5, 'OD604', '5', 'OD604.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (6, 'OD608', '5', 'OD608.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (7, 'IJ-304', '6', 'IJ-304.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (8, 'IJ-309', '6', 'IJ-309.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (9, 'IJ-314', '6', 'IJ-314.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (10, 'CB-107', '8', 'CB-107.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (11, 'CB-112', '8', 'CB-112.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (12, 'CB-120', '8', 'CB-120.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (13, 'LP-901', '9', 'LP-901.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (14, 'LP-907', '9', 'LP-907.jpg', 1, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (15, 'LP-915', '9', 'LP-915.jpg', 1, 15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (16, 'OR-702', '10', 'OR-702.jpg', 1, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (17, 'OR-704', '10', 'OR-704.jpg', 1, 17, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `top_product` VALUES (18, 'OR-709', '10', 'OR-709.jpg', 1, 18, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int unsigned NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int unsigned NULL,
  `remember_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int unsigned NOT NULL,
  `last_login` int unsigned NULL,
  `active` tinyint unsigned NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_email`(`email`) USING BTREE,
  UNIQUE INDEX `uc_activation_selector`(`activation_selector`) USING BTREE,
  UNIQUE INDEX `uc_forgotten_password_selector`(`forgotten_password_selector`) USING BTREE,
  UNIQUE INDEX `uc_remember_selector`(`remember_selector`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2y$12$uu0p2asmZ4exXnvB4.L0bObqQDgqxsz0dMnU3GisU6oZd6v55yNEa', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1647269919, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  `group_id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (2, 1, 2);

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : indojayapvc

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 22/02/2022 23:41:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for category_product
-- ----------------------------
DROP TABLE IF EXISTS `category_product`;
CREATE TABLE `category_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `link` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keyword` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(2) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category_product
-- ----------------------------
INSERT INTO `category_product` VALUES (1, 'Atap Bajaringan', 'atapbajaringan', 'Atap Bajaringan', 0, 1, NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `category_product` VALUES (2, 'Plafon Gypsum', 'plafongypsum', 'Plafon Gypsum', 0, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (3, 'List Gypsum', 'listgypsum', 'List Gypsum', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (4, 'Partisi Gypsum', 'partisigypsum', 'Partisi Gypsum', 0, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (5, 'Moulding', 'moulding', 'Moulding', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (6, 'Plafon PVC', 'plafonpvc', 'Plafon PVC', 0, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (7, 'Partisi PVC', 'partisipvc', 'Partisi PVC', 0, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (8, 'Cutting Board PVC', 'cuttingboardpvc', 'Cutting Board PVC', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (9, 'Lis PVC', 'lispvc', 'Lis PVC', 0, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `category_product` VALUES (10, 'Ornamen PVC  ', 'ornamenpvc', 'Ornamen PVC  ', 0, 10, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'Dashboard', 'Dashboard', 'fa fa-arrow-circle-right', 1, 'dashboard', 'DSB', NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `menu` VALUES (2, 'Menu', 'Menu', 'fa fa-arrow-circle-right', 2, 'menu', 'MS', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (3, 'Menu Category', 'Menu Category', 'fa fa-arrow-circle-right', 3, 'menucategory', 'MS', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menu_action
-- ----------------------------
DROP TABLE IF EXISTS `menu_action`;
CREATE TABLE `menu_action`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `menu_id` int(20) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_action
-- ----------------------------
INSERT INTO `menu_action` VALUES (1, 'add', 'fa fa-arrow-circle-right', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_action` VALUES (2, 'edit', 'fa fa-arrow-circle-right', 2, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_action` VALUES (3, 'delete', 'fa fa-arrow-circle-right', 3, 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menu_category
-- ----------------------------
DROP TABLE IF EXISTS `menu_category`;
CREATE TABLE `menu_category`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `kode` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_category
-- ----------------------------
INSERT INTO `menu_category` VALUES (1, 'DSB', 'Dashboard', 'Dashboard', 'fas fa-ellipsis-h', 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3932 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (3931, 1, 1, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for permission_action
-- ----------------------------
DROP TABLE IF EXISTS `permission_action`;
CREATE TABLE `permission_action`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `permission_id` int(20) NOT NULL,
  `menu_action_id` int(20) NULL DEFAULT NULL,
  `group_id` int(20) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission_action
-- ----------------------------
INSERT INTO `permission_action` VALUES (38, 1, 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission_action` VALUES (39, 1, 2, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permission_action` VALUES (40, 1, 3, 3, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_product` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `category` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(2) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (23, 'CB-101', '8', 'CB-101.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (24, 'CB-102', '8', 'CB-102.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (25, 'CB-103', '8', 'CB-103.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (26, 'CB-104', '8', 'CB-104.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (27, 'CB-105', '8', 'CB-105.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (28, 'CB-106', '8', 'CB-106.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (29, 'CB-107', '8', 'CB-107.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (30, 'CB-108', '8', 'CB-108.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (31, 'CB-109', '8', 'CB-109.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (32, 'CB-110', '8', 'CB-110.jpg', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (33, 'CB-111', '8', 'CB-111.jpg', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (34, 'CB-112', '8', 'CB-112.jpg', 1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (35, 'CB-113', '8', 'CB-113.jpg', 1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (36, 'CB-114', '8', 'CB-114.jpg', 1, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (37, 'CB-115', '8', 'CB-115.jpg', 1, 15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (38, 'CB-116', '8', 'CB-116.jpg', 1, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (39, 'CB-117', '8', 'CB-117.jpg', 1, 17, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (40, 'CB-118', '8', 'CB-118.jpg', 1, 18, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (41, 'CB-119', '8', 'CB-119.jpg', 1, 19, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (42, 'CB-120', '8', 'CB-120.jpg', 1, 20, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (43, 'C 505 S', '6', 'C 505 S.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (44, 'C 506 S', '6', 'C 506 S.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (45, 'C 510 K', '6', 'C 510 K.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (46, 'C 511 K', '6', 'C 511 K.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (47, 'C 512 K', '6', 'C 512 K.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (48, 'OD601', '5', 'OD601.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (49, 'OD602', '5', 'OD602.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (50, 'OD603', '5', 'OD603.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (51, 'OD604', '5', 'OD604.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (52, 'OD605', '5', 'OD605.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (53, 'OD606', '5', 'OD606.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (54, 'OD607', '5', 'OD607.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (55, 'OD608', '5', 'OD608.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (56, 'OD609', '5', 'OD609.jpg', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (57, 'C 505 S', '3', 'C 505 S.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (58, 'C 506 S', '3', 'C 506 S.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (59, 'C 509 K', '3', 'C 509 K.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (60, 'C 510 K', '3', 'C 510 K.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (61, 'C 511 K', '3', 'C 511 K.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (62, 'C 512 K', '3', 'C 512 K.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (63, 'C 501 B', '8', 'C501B.jpg', 1, 21, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `product` VALUES (64, 'C 502 B', '8', 'C502B.jpg', 1, 22, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_project` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `company` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(2) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES (14, 'Four Points Makassar', 'Jl. Andi Djemma No.130, Banta-Bantaeng, Kec. Rappocini, Kota Makassar', 'PT. IMB', 'fourpoint.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (15, 'Bank BCA KCU Panakkukang', 'Jl. Boulevard No.5, Pisang Utara, Kec. Ujung Pandang, Kota Makassar', 'PT. Pulau Intan', 'BCA.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (16, 'Bank Indonesia Cabang Sul-Sel', 'Jl. Jend. Sudirman No.3, Pisang Utara, Kec. Ujung Pandang, Kota Makassar', 'PT. Nindya Karya', 'BI.jpg', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (17, 'RSU KOLAKA', 'Jl. Mekongga Indah, Tahoa, Kec. Kolaka, Kabupaten Kolaka', 'PT. Brantas Abibraya', 'RSKolaka.jpg', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (19, 'RS Tadjuddin', NULL, 'PT. PP Urban', 'RSTadjuddin.jpg', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (20, 'Bandara EL Tari', NULL, 'PT. Nindya Karya', 'ELTari.jpg', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (21, 'Poltekpar Lombok', NULL, 'PT. Nindya Karya', 'Poltekpar.jpg', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (22, 'Nipah Mall', NULL, 'PT. PP Persero', 'NipahMall.jpg', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (23, 'Bandara Samratulangi Manado', NULL, 'PT. Adhi Karya', 'Samratulangi.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `project` VALUES (24, 'Gudang Indofood', NULL, 'PT. Total Persada Indonesia', 'Indofood.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contac` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field1` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field2` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field3` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES (1, '6282344444422', NULL, NULL, NULL, NULL, NULL, '2021-06-23 01:57:32', 1, NULL);

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `link` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hilight` tinyint(1) NULL DEFAULT NULL,
  `position` int(2) NULL DEFAULT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` enum('Desktop','Mobile','') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `user_created` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `user_update` int(11) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES (14, 'WEB.png', 'moulding', 1, 2, 'pesan sekarang', 'Desktop', NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `slider` VALUES (15, 'WEB2.jpg', 'cuttingboardpvc', 1, 3, 'pesan sekarang', 'Desktop', NULL, NULL, '2021-06-23 01:57:32', 1, NULL);
INSERT INTO `slider` VALUES (16, 'SliderHome.jpg', '#', 1, 1, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (17, 'SliderHomeMobile.jpg', '#', 1, 1, 'pesan sekarang', 'Mobile', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (18, 'KTG1.jpg', 'plafonpvc', 1, 4, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (19, 'KTG2.jpg', 'plafonpvc', 1, 5, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (20, 'KTG3.jpg', 'plafonpvc', 1, 6, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `slider` VALUES (21, 'KTG4.jpg', 'plafonpvc', 1, 7, 'pesan sekarang', 'Desktop', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_email`(`email`) USING BTREE,
  UNIQUE INDEX `uc_activation_selector`(`activation_selector`) USING BTREE,
  UNIQUE INDEX `uc_forgotten_password_selector`(`forgotten_password_selector`) USING BTREE,
  UNIQUE INDEX `uc_remember_selector`(`remember_selector`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2y$12$uu0p2asmZ4exXnvB4.L0bObqQDgqxsz0dMnU3GisU6oZd6v55yNEa', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1642347472, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (2, 1, 2);

SET FOREIGN_KEY_CHECKS = 1;

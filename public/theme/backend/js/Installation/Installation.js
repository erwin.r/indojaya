
var loadData = base_url + "/installation/data",
tableID = $("#data-table-book"),
Page = function () {
      return {
         init: function () {
            Page.main();
            Page.tableAjax();
         },
         tableAjax: function () {
            $.ajaxSetup({
                  headers: {
                     'X-CSRF-TOKEN': $('meta[name="X-CSRF-TOKEN"]').attr('content')
                  }
            });

            //datatables
            tableID.DataTable({
                  lengthMenu: [10, 20, 50, 100],
                  pageLength: 10,
                  language: {
                     'lengthMenu': 'Display _MENU_',
                  },
                  searchDelay: 500,
                  responsive: true,
                  autoWidth: false,
                  serverSide : true,
                  processing: true,
                  searching: false,
                  order: [[1, 'asc']],
                  columnDefs: [{
                     orderable: false,
                     targets: [0]
                  },{ responsivePriority: 3, targets: -1 }],

                  ajax : {
                     url: loadData,
                     method : 'POST',
                     data: function (d) {
                        // get value for search
                        d.image = $('#image').val();
                        d.description = $('#description').val();
                        d.position = $('#position').val();
                        
                    }
                  }

            });

            $('#kt_search').click(function () { //button filter event click
                  tableID.DataTable().draw(); //just reload table
            });

         },
         main: function () {
            $("#modal-create-book").on("hidden.bs.modal", function() {
                  $(this).find("#form-create-book")[0].reset();
                  $(".text-danger").remove();
                  $(".is-invalid").removeClass("is-invalid");
            });

            $(document).on("click", "#create", function () {
                  Page.add()
            });

            $(document).on("submit", "#form-add", function () {
                  return Page.submitForm($("#form-add"), base_url + "/installation/addnew"), !1
            });

            $(document).on("click", ".btn-edit", function () {
                  var e = $(this).attr("data-id");
                  Page.edit(e)
            });

            $(document).on("submit", "#form-edit", function () {
                  var id = $("#book_id").val();
                  return Page.submitForm($("#form-edit"), base_url + "/installation/updated"), !1
            });

            $(document).on("click", ".btn-delete", function (e) {
                  Swal.fire({
                     title: "Are you sure?",
                     text: "You won't be able to revert this!",
                     icon: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#3085d6",
                     cancelButtonColor: "#d33",
                     confirmButtonText: "Yes, delete it!"
                  })
                  .then((result) => {
                     if (result.value) {
                        $.ajax({
                              url: base_url + "/installation/resources/"+$(this).attr("data-id"),
                              method: "DELETE",
                        }).done((data, textStatus) => {
                              swal.fire({
                                 icon: "success",
                                 title: textStatus,
                              });
                              tableID.DataTable().ajax.reload();
                        }).fail((error) => {
                              swal.fire({
                                 icon: "error",
                                 title: error.responseJSON.messages.error,
                              });
                        })
                     }
                  })
            });
         },
         add: function () {
            var a = Helper.loadModal("lg"),
                  i = a.find(".modal-body"),
                  t = a.find(".modal-title");
            t.text("Add Data"), 
            Helper.blockElement($(i)), 
            $.ajax({
                  url: base_url + "/installation/add",
                  method: "GET",
            })
            .done(function (e) {
                  i.html(e);
                  Helper.unblockElement($(i));
                  Page.AttacFile();
            })
         },
         edit: function (e) {
            var a = Helper.loadModal("lg"),
                  i = a.find(".modal-body"),
                  t = a.find(".modal-title");
            t.text("Edit Data"), 
            Helper.blockElement($(i)), 
            $.ajax({
                  url: base_url + "/installation/edit",
                  method: "GET",
                  data: {id:e}, 
            })
            .done(function (e) {
                  i.html(e);
                  Helper.unblockElement($(i));
                  Page.AttacFile();

                  var file=document.getElementById('AttachedDocument').value;
                      mockFile = { name: file, size: 0 };
                  myDropzone.displayExistingFile(mockFile, "getfile?path=installation&image=" + file);

                  $('.dz-remove').on('click', function () {
                        document.getElementById('AttachedDocument').value='';
                  }); 
            })
         },
         AttacFile: function () {
            myDropzone = new Dropzone("#kt_dropzone_1", { 
                  url: base_url + "/installation/import", // Set the url for your upload script location
                  // url: base_url + "/public/theme/frontend/img/product/cuttingboardpvc", // Set the url for your upload script location
                  paramName: "file", // The name that will be used to transfer the file
                  maxFiles: 1,
                  maxFilesize: 5, // MB
                  addRemoveLinks: true,
                  acceptedFiles: ".png,.jpeg,.jpg",
                  headers: { 'X-CSRF-TOKEN': $('meta[name="X-CSRF-TOKEN"]').attr('content') },
                  accept: function (file, done) {
                        done();                           
                        this.on('addedfile', function(file) {
                              if (this.files.length > 1) {
                                    this.removeFile(this.files[0]);
                              }
                        });                                          
                        this.on("success", function (file) {        
                              document.getElementById('AttachedDocument').value=file.xhr.responseText;
                              $('.dz-remove').on('click', function () {
                                    document.getElementById('AttachedDocument').value='';
                              });
                        });
                  }
            });
         },
         submitForm: function (e, url) {
            var i = e.find(".submit");
            Helper.blockElement(e.parent()), i.attr("disabled", !0);
            $.ajax({
                  url: url,
                  headers: {
                        'X-CSRF-TOKEN': $('meta[name="X-CSRF-TOKEN"]').attr('content')
                  },
                  type: "POST", //form method
                  data: e.serialize()
            }).done((data, textStatus) => {
                  swal.fire({
                     icon: "success",
                     title: textStatus
                  })
                  $(".closed").click();
                  $(tableID).DataTable().ajax.reload();
                  e.trigger("reset");
                  Helper.blockElement(e.parent()), i.removeAttr("disabled", !0);
            }).fail((xhr, status, error) => {
                  Helper.unblockElement(e.parent());
                  $.each(xhr.responseJSON.messages, function (key, value) {
                     $("input[name=" + key + "]").addClass('is-invalid');
                     $("." + key).text(value);
                     Helper.removeValidate(key);
                  });
                  i.removeAttr("disabled");
            });
         }
      }
}();
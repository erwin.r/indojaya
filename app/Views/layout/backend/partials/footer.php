<footer class="footerclass">
    <div class="footer-nav-wrapper">
        <a href="#" class="footer-nav-single">
            <div class="menu-wrapper">
                <i class="fas fa-lg fa-home font-green"></i>
                <span>Home</span>
            </div>
        </a>
        <a href="#" class="footer-nav-single">
            <div class="menu-wrapper">
                <i class="fab fa-lg fa-facebook-messenger font-green"></i>
                <span>Chat</span>
            </div>
        </a>
        <a href="#" class="footer-nav-single">
            <div class="menu-wrapper">
                <i class="fas fa-lg fa-search-plus  font-green"></i>
                <span>Query</span>
            </div>
        </a>
        <a href="<?= base_url('auth/profile/'.$this->session->get('user_id')); ?>" class="footer-nav-single">
            <div class="menu-wrapper">
                <i class="fas fa-lg fa-user font-green"></i>
                <span>Profile</span>
            </div>
        </a>
        <!-- <a href="javascript:;" class="footer-nav-single menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <div class="menu-wrapper">
                <i class="fa fa-th-large fa-lg font-green"></i>
                <span>Menu</span>
            </div>
        </a> -->
    </div>
</footer>
<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2"><?php echo date('Y'); ?></span>
            <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">© Copyright by CICT</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Nav-->
        <!-- <div class="nav nav-dark order-1 order-md-2">
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pr-3 pl-0">About</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link px-3">Team</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-3 pr-0">Contact</a>
        </div> -->
        <!--end::Nav-->
    </div>
    <!--end::Container-->
</div>
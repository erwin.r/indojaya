<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS"); 
    ?>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Indojaya</title>
    <meta name="description" content="Produk dan Jasa Pemasangan Atap Bajaringan, Plafon Gypsum, List Gypsum, Partisi Gypsum, Moulding, Plafon PVC, Partisi PVC, Cutting Board PVC, Lis PVC, Ornamen PVC">
    <meta name="keyword" content="Produk dan Jasa Pemasangan Atap Bajaringan, Plafon Gypsum, List Gypsum, Partisi Gypsum, Moulding, Plafon PVC, Partisi PVC, Cutting Board PVC, Lis PVC, Ornamen PVC">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/theme/frontend/img/favicon.png">

    <!-- all css here -->
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/animate.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/icofont.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/meanmenu.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/bundle.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>/theme/frontend/css/responsive.css">
    <script src="<?= base_url() ?>/theme/frontend/js/vendor/modernizr-3.11.7.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/lazysizes.min.js"></script>    
</head>

<body>    
    <header>        
        <div class="header-bottom pt-40 pb-30 clearfix">
            <div class="header-bottom-wrapper pr-200 pl-200">
                <div class="logo-3">
                    <a href="<?= base_url() ?>">
                        <img src="<?= base_url() ?>/theme/frontend/img/logo/logo-3.png" alt="">
                    </a>
                </div>
                <div class="mobile-menu-area electro-menu d-md-block col-md-12 col-lg-12 col-12 d-lg-none d-xl-none">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul class="menu-overflow">
                                <li><a href="<?= base_url(); ?>">Beranda</a></li>
                                <li><a href="<?= base_url(); ?>/proyek">Proyek</a></li>
                                <li><a href="<?= base_url(); ?>/tentang">Tentang</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>                
                <form method="GET" action="<?= base_url() ?>/cari-produk">
                    <div class="categories-wrapper">
                        <div class="categories-search-wrapper">
                            <div class="all-categories">
                                <div class="select-wrapper">
                                <?php 
                                    $this->ionAuth = new \IonAuth\Libraries\IonAuth();    
                                    $category = $this->ionAuth->getCategoryProduct('1=1')->getResult();
                                    $setting = $this->ionAuth->getSetting();
                                ?>
                                    <select name="kategori" class="select">
                                        <option value="semua">Semua Kategori</option>
                                        <?php foreach ($category as $r) {?>        
                                            <option value="<?php echo $r->link; ?>"><?php echo $r->category; ?></option>
                                        <?php } ?>                                        
                                    </select>
                                </div>
                            </div>                
                            <!-- <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" /> -->
                            <input name="query" placeholder="Masukkan Kata Kunci" type="text" class="input-search">
                            <button style="position: relative;" type="submit"> Cari </button>
                        </div>
                    </div>                
                </form>
            </div>
        </div>
    </header>
    <div class="pl-200 pr-200 overflow clearfix">
        <div class="categori-menu-slider-wrapper clearfix">
            <div class="categories-menu">
                <div class="category-heading">
                    <h3> Produk </h3>
                    <?php $uri = service('uri');
                        //  echo $module = $uri->getSegment(1); ?> 
                </div>
                <?= $this->include('layout/frontend/category') ?>                    
            </div>
            <div class="menu-slider-wrapper">
                <?= $this->include('layout/frontend/menu') ?>                                
                <div class="d-none d-lg-block"> <!-- slider desktop -->
                    <div class="slider-area">
                        <div class="slider-active owl-carousel">    
                            <?php 
                                if ($uri->getSegment(1) == '' or $uri->getSegment(1) == 'proyek' or $uri->getSegment(1) == 'tentang' or $uri->getSegment(1) == 'cari-produk') 
                                    $slider = $this->ionAuth->getSlider('hilight=1 AND type="Desktop"')->getResult(); 
                                    else 
                                    $slider = $this->ionAuth->getSlider('link="'.$uri->getSegment(2).'" AND type="Desktop"')->getResult(); 
                                foreach ($slider as $r) {
                            ?>                                                                            
                                <div class="single-slider single-slider-hm3 bg-img pt-170 pb-173 img-responsive" style="background-repeat:no-repeat; background-size:100% 100%; background-image: url(<?= (!empty($r->image)) ? base_url("getfile?path=slider&image=" . $r->image) : "" ?>)">                                

                                    <div class="slider-animation slider-content-style-3 fadeinup-animated">
                                        <h2 class="animated">&nbsp;</h2>
                                        <h4 class="animated">&nbsp;</h4>
                                        <a class="electro-slider-btn btn-hover" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $r->message; ?>">Pesan Sekarang</a>
                                    </div>
                                </div>
                            <?php } ?>    
                        </div>      
                    </div>
                </div>   
                <div class="d-lg-none"> <!-- slider mobile -->
                    <div class="slider-area">
                        <div class="slider-active owl-carousel">  
                            <?php 
                                if ($uri->getSegment(1) == '' or $uri->getSegment(1) == 'proyek' or $uri->getSegment(1) == 'tentang' or $uri->getSegment(1) == 'cari-produk') 
                                    $slider = $this->ionAuth->getSlider('hilight=1 AND type="Mobile"')->getResult(); 
                                    else 
                                    $slider = $this->ionAuth->getSlider('link="'.$uri->getSegment(2).'" AND type="Mobile"')->getResult(); 
                                foreach ($slider as $r) {
                            ?>                                                                            
                                <div class="single-slider single-slider-hm3 bg-img pt-170 pb-173" style="background-repeat:no-repeat; background-size:100% 100%; background-image: url(<?= (!empty($r->image)) ? base_url("getfile?path=slider&image=" . $r->image) : "" ?>)">
                                    <div class="slider-animation slider-content-style-3 fadeinup-animated">
                                        <h2 class="animated">&nbsp;</h2>
                                        <h4 class="animated">&nbsp;</h4>
                                        <a class="electro-slider-btn btn-hover" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $r->message; ?>">Pesan Sekarang</a>
                                    </div>
                                </div>
                            <?php } ?> 
                        </div>                                            
                    </div>
                </div>
            </div>
        </div>
    </div>
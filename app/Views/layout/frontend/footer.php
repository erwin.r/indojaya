<style>
    .wa{position:fixed;bottom:5px;right:2px;z-index:999;cursor: pointer;}
    .whatsappBlock a{display:inline-block;height:70px;padding:5px 10px;color:#65BC54 !important;font-weight:bold}
    .whatsappBlock a img{height:100%;width:auto}
</style>

    <footer class="footer-area">
        <div class="footer-top-3 black-bg pt-75 pb-25">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-xl-3">
                        <div class="footer-widget mb-40">
                            <h3 class="footer-widget-title-3">Alamat</h3>
                            <div class="footer-info-wrapper-2">
                                <div class="footer-address-electro">                                    
                                    <div class="footer-info-content2">
                                        <p>Jl. DR. Leimena No.77
                                        <br>Kota Makassar, Sulawesi Selatan 90144</p>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xl-3">
                        <div class="footer-widget mb-40">
                            <h3 class="footer-widget-title-3">Telepon</h3>
                            <div class="footer-info-wrapper-2">                                
                                <div class="footer-address-electro">
                                    <div class="footer-info-content2">
                                        <?php 
                                            $this->ionAuth = new \IonAuth\Libraries\IonAuth();
                                            $setting = $this->ionAuth->getSetting();
                                        ?>
                                        <p><a href="tel:<?= $setting->contac; ?>"><?= $setting->contac; ?></a>
                                       <br><a href="tel:<?= $setting->field1; ?>"><?= $setting->field1; ?></a></p>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xl-3">
                        <div class="footer-widget mb-40">
                            <h3 class="footer-widget-title-3">Email</h3>
                            <div class="footer-info-wrapper-2">
                                <div class="footer-address-electro">
                                    <div class="footer-info-content2">
                                        <p><a href="mailto:ptindojaya99@gmail.com">ptindojaya99@gmail.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    
                    <!-- <div class="col-lg-3 col-md-6 col-xl-3">
                        <div class="footer-widget mb-40">
                            <h3 class="footer-widget-title-3">Langganan</h3>
                            <form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" novalidate>
                                <div id="mc_embed_signup_scroll" class="mc-form input-group">
                                    <input type="email" value="" name="EMAIL" class="email input-subscriber" placeholder="Masukkan E-mail Anda" required>                                                                
                                    <input type="submit" value="Simpan" name="subscribe" id="mc-embedded-subscribe" class="button">                                    
                                </div>
                            </form>
                        </div>  
                    </div>   -->
                </div>
            </div>
        </div>   
        <div class="footer-bottom  black-bg-2 pt-25 pb-30">
            <div class="container">
                <div class="row">                    
                    <div class="col-lg-12 col-md-12">
                        <div class="copyright f-right mrg-5">
                            <p>
                                Copyright ©
                                <a href="https://wa.me/6285342131946/">ER </a><?php echo date('Y'); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <?php $setting = $this->ionAuth->getSetting();?>
        <div class="wa whatsappBlock" style="width: 77px; height: 70px; text-align: right; font-size: 25px; margin-right: 5px;">
            <a href="https://wa.me/<?= $setting->contac; ?>?text=AssalamuAlaikum" style="width: 100%;">
                <img src="<?= base_url() ?>/theme/frontend/img/whatsapp.png" alt="WhatsApp" width="24px" height="24px">
            </a>
        </div>    
    </footer>
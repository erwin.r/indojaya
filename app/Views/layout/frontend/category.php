<?php 
    $uri = service('uri');
    if($uri->getSegment(1) == 'produk') @$segmen2=$uri->getSegment(2); else $segmen2='';
    $this->ionAuth = new \IonAuth\Libraries\IonAuth();    
    $category = $this->ionAuth->getCategoryProduct('1=1')->getResult();
?>
<div class="category-menu-list">
    <ul>     
        <?php foreach ($category as $r) {?>        
            <li>
                <a <?php if(@$segmen2 == $r->link) echo 'style="color:#130f40"' ?> href="<?php echo base_url()."/produk/".$r->link; ?>"><img alt="" src="<?= base_url() ?>/theme/frontend/img/icon-img/24.png"><?php echo $r->category; ?></a>
            </li>
        <?php } ?>        
    </ul>
</div>
<?= $this->extend('layout/backend/main') ?>

<?= $this->section('content') ?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<!--begin::Subheader-->
	<div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
		<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center mr-1">
				<!--begin::Page Heading-->
				<div class="d-flex align-items-baseline flex-wrap mr-5">
					<!--begin::Page Title-->
					<h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Dashboard After Sales</h2>
					<!--end::Page Title-->
				</div>
				<!--end::Page Heading-->
			</div>
			<!--end::Info-->
		</div>
	</div>
	<!--end::Subheader-->
	<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<div class="container">
			<!--begin::Dashboard-->
			<!--begin::Row-->
			<div class="row">
				<div class="col-xl-4">
					<!--begin::Mixed Widget 4-->
					<div class="card card-custom bg-radial-gradient-success gutter-b card-stretch">
						<!--begin::Header-->
						<div class="card-header border-0 py-5">
							<h3 class="card-title font-weight-bolder text-white">Area West</h3>
							<div class="card-toolbar">
								<div class="dropdown dropdown-inline">
									<a href="#" class="btn btn-text-white btn-hover-white btn-sm btn-icon border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header pb-1">
												<span class="text-primary text-uppercase font-weight-bold font-size-sm">Add new:</span>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-shopping-cart-1"></i>
													</span>
													<span class="navi-text">Order</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-calendar-8"></i>
													</span>
													<span class="navi-text">Event</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-graph-1"></i>
													</span>
													<span class="navi-text">Report</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-rocket-1"></i>
													</span>
													<span class="navi-text">Post</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-writing"></i>
													</span>
													<span class="navi-text">File</span>
												</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body d-flex flex-column p-0">
							<!--begin::Chart-->
							<div id="kt_mixed_widget_4_chart" style="height: 200px"></div>
							<!--end::Chart-->
							<!--begin::Stats-->
							<div class="card-spacer bg-white card-rounded flex-grow-1">
								<!--begin::Row-->
								<div class="row m-0">
									<div class="col px-8 py-6 mr-8">
										<div class="font-size-sm text-muted font-weight-bold">CPUS</div>
										<div class="font-size-h4 font-weight-bolder">116%</div>
									</div>
									<div class="col px-8 py-6">
										<div class="font-size-sm text-muted font-weight-bold">Service Share</div>
										<div class="font-size-h4 font-weight-bolder">+16%</div>
									</div>
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row m-0">
									<div class="col px-8 py-6 mr-8">
										<div class="font-size-sm text-muted font-weight-bold">PM Rate</div>
										<div class="font-size-h4 font-weight-bolder">110%</div>
									</div>
									<div class="col px-8 py-6">
										<div class="font-size-sm text-muted font-weight-bold">Part Sales</div>
										<div class="font-size-h4 font-weight-bolder">107%</div>
									</div>
								</div>
								<!--end::Row-->
							</div>
							<!--end::Stats-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::Mixed Widget 4-->
				</div>
				<div class="col-xl-4">
					<!--begin::Mixed Widget 4-->
					<div class="card card-custom bg-radial-gradient-warning gutter-b card-stretch">
						<!--begin::Header-->
						<div class="card-header border-0 py-5">
							<h3 class="card-title font-weight-bolder text-white">Area East</h3>
							<div class="card-toolbar">
								<div class="dropdown dropdown-inline">
									<a href="#" class="btn btn-text-white btn-hover-white btn-sm btn-icon border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header pb-1">
												<span class="text-primary text-uppercase font-weight-bold font-size-sm">Add new:</span>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-shopping-cart-1"></i>
													</span>
													<span class="navi-text">Order</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-calendar-8"></i>
													</span>
													<span class="navi-text">Event</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-graph-1"></i>
													</span>
													<span class="navi-text">Report</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-rocket-1"></i>
													</span>
													<span class="navi-text">Post</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-writing"></i>
													</span>
													<span class="navi-text">File</span>
												</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body d-flex flex-column p-0">
							<!--begin::Chart-->
							<div id="kt_mixed_widget_5_chart" style="height: 200px"></div>
							<!--end::Chart-->
							<!--begin::Stats-->
							<div class="card-spacer bg-white card-rounded flex-grow-1">
								<!--begin::Row-->
								<div class="row m-0">
									<div class="col px-8 py-6 mr-8">
										<div class="font-size-sm text-muted font-weight-bold">CPUS</div>
										<div class="font-size-h4 font-weight-bolder">94%</div>
									</div>
									<div class="col px-8 py-6">
										<div class="font-size-sm text-muted font-weight-bold">Service Share</div>
										<div class="font-size-h4 font-weight-bolder">-6%</div>
									</div>
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row m-0">
									<div class="col px-8 py-6 mr-8">
										<div class="font-size-sm text-muted font-weight-bold">PM Rate</div>
										<div class="font-size-h4 font-weight-bolder">110%</div>
									</div>
									<div class="col px-8 py-6">
										<div class="font-size-sm text-muted font-weight-bold">Part Sales</div>
										<div class="font-size-h4 font-weight-bolder">108%</div>
									</div>
								</div>
								<!--end::Row-->
							</div>
							<!--end::Stats-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::Mixed Widget 4-->
				</div>
				<div class="col-xl-4">
					<!--begin::Mixed Widget 4-->
					<div class="card card-custom bg-radial-gradient-primary gutter-b card-stretch">
						<!--begin::Header-->
						<div class="card-header border-0 py-5">
							<h3 class="card-title font-weight-bolder text-white">Head Office</h3>
							<div class="card-toolbar">
								<div class="dropdown dropdown-inline">
									<a href="#" class="btn btn-text-white btn-hover-white btn-sm btn-icon border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-hor"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
										<!--begin::Navigation-->
										<ul class="navi navi-hover">
											<li class="navi-header pb-1">
												<span class="text-primary text-uppercase font-weight-bold font-size-sm">Add new:</span>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-shopping-cart-1"></i>
													</span>
													<span class="navi-text">Order</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-calendar-8"></i>
													</span>
													<span class="navi-text">Event</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-graph-1"></i>
													</span>
													<span class="navi-text">Report</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-rocket-1"></i>
													</span>
													<span class="navi-text">Post</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="#" class="navi-link">
													<span class="navi-icon">
														<i class="flaticon2-writing"></i>
													</span>
													<span class="navi-text">File</span>
												</a>
											</li>
										</ul>
										<!--end::Navigation-->
									</div>
								</div>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body d-flex flex-column p-0">
							<!--begin::Chart-->
							<div id="kt_mixed_widget_6_chart" style="height: 200px"></div>
							<!--end::Chart-->
							<!--begin::Stats-->
							<div class="card-spacer bg-white card-rounded flex-grow-1">
								<!--begin::Row-->
								<div class="row m-0">
									<div class="col px-8 py-6 mr-8">
										<div class="font-size-sm text-muted font-weight-bold">CPUS</div>
										<div class="font-size-h4 font-weight-bolder">120%</div>
									</div>
									<div class="col px-8 py-6">
										<div class="font-size-sm text-muted font-weight-bold">Service Share</div>
										<div class="font-size-h4 font-weight-bolder">+20%</div>
									</div>
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row m-0">
									<div class="col px-8 py-6 mr-8">
										<div class="font-size-sm text-muted font-weight-bold">PM Rate</div>
										<div class="font-size-h4 font-weight-bolder">112%</div>
									</div>
									<div class="col px-8 py-6">
										<div class="font-size-sm text-muted font-weight-bold">Part Sales</div>
										<div class="font-size-h4 font-weight-bolder">105%</div>
									</div>
								</div>
								<!--end::Row-->
							</div>
							<!--end::Stats-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::Mixed Widget 4-->
				</div>
			</div>
			<!--end::Row-->
			<div class="row">
				<div class="col-xl-12">
					<!--begin::Card-->
					<div class="card card-custom gutter-b">
						<div class="card-header">
							<div class="card-title">
								<h3 class="card-label">Summary</h3>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-xl-4">
									<div class="row justify-content-md-center">
										<div class="col-md-auto">
											<div class="text-center font-size-lg text-muted font-weight-bold">Service Share Achievement</div>
											<div class="text-center font-size-h1 font-weight-bolder">-8%</div>
										</div>
									</div>
									<div class="row">
										<div class="col">
											<div class="text-center font-size-sm text-muted font-weight-bold">Actual</div>
											<div class="text-center font-size-h4 font-weight-bolder">92%</div>
										</div>
										<div class="col">
											<div class="text-center font-size-sm text-muted font-weight-bold">Target</div>
											<div class="text-center font-size-h4 font-weight-bolder">100%</div>
										</div>
									</div>
								</div>
								<div class="col-xl-8">
									<!--begin::Chart-->
									<div id="chart_5"></div>
									<!--end::Chart-->
								</div>
							</div>
						</div>
					</div>
					<!--end::Card-->
				</div>
			</div>
			<!--begin::Row-->
			<div class="row">
				<div class="col-xl-4">
					<div class="row">
						<div class="col-xl-12">
							<!--begin::Stats Widget 11-->
							<div class="card card-custom gutter-b">
								<!--begin::Body-->
								<div class="card-body p-0">
									<div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
										<span class="symbol symbol-circle symbol-50 symbol-light-danger mr-2">
											<span class="symbol-label">
												<span class="svg-icon svg-icon-xl svg-icon-danger">
													<!--begin::Svg Icon | path:<?= base_url() ?>/theme/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
															<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
														</g>
													</svg>
													<!--end::Svg Icon-->
												</span>
											</span>
										</span>
										<div class="d-flex flex-column text-right">
											<span class="text-dark-75 font-weight-bolder font-size-h3">750</span>
											<span class="text-muted font-weight-bold mt-2">Weekly Work Order</span>
										</div>
									</div>
									<div id="kt_stats_widget_11_chart" class="card-rounded-bottom" data-color="danger" style="height: 150px"></div>
								</div>
								<!--end::Body-->
							</div>
							<!--end::Stats Widget 11-->
						</div>
						<div class="col-xl-12">
							<!--begin::Stats Widget 10-->
							<div class="card card-custom gutter-b">
								<!--begin::Body-->
								<div class="card-body p-0">
									<div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
										<span class="symbol symbol-circle symbol-50 symbol-light-info mr-2">
											<span class="symbol-label">
												<span class="svg-icon svg-icon-xl svg-icon-info">
													<!--begin::Svg Icon | path:<?= base_url() ?>/theme/assets/media/svg/icons/Shopping/Cart3.svg-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24" />
															<path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
															<path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000" />
														</g>
													</svg>
													<!--end::Svg Icon-->
												</span>
											</span>
										</span>
										<div class="d-flex flex-column text-right">
											<span class="text-dark-75 font-weight-bolder font-size-h3">+259</span>
											<span class="text-muted font-weight-bold mt-2">Part</span>
										</div>
									</div>
									<div id="kt_stats_widget_10_chart" class="card-rounded-bottom" data-color="info" style="height: 150px"></div>
								</div>
								<!--end::Body-->
							</div>
							<!--end::Stats Widget 10-->
						</div>
					</div>
				</div>
				<div class="col-xl-8">
					<!--begin::List Widget 14-->
					<div class="card card-custom gutter-b card-stretch">
						<!--begin::Header-->
						<div class="card-header border-0">
							<h3 class="card-title font-weight-bolder text-dark">Top 5 Branch</h3>
							<div class="card-toolbar">
								<div class="dropdown dropdown-inline">
									<a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="ki ki-bold-more-ver"></i>
									</a>
								</div>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body pt-2">
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
									<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">HK Urip Sumoharjo</a>
									<span class="text-muted font-weight-bold font-size-sm my-1">Jln Kebangsaan</span>
								</div>
								<!--end::Title-->
								<!--begin::Info-->
								<div class="d-flex align-items-center py-lg-0 py-2">
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-weight-bolder font-size-h4">24,900</span>
										<span class="text-muted font-size-sm font-weight-bolder">Work Order & Parts</span>
									</div>
								</div>
								<!--end::Info-->
							</div>
							<!--end::Item-->
							<!--begin: Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
									<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">HK Kendari</a>
									<span class="text-muted font-weight-bold font-size-sm my-1">Jln Kebangsaan</span>
								</div>
								<!--end::Title-->
								<!--begin::Info-->
								<div class="d-flex align-items-center py-lg-0 py-2">
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-weight-bolder font-size-h4">70,380</span>
										<span class="text-muted font-weight-bolder font-size-sm">Work Order & Parts</span>
									</div>
								</div>
								<!--end::Info-->
							</div>
							<!--end: Item-->
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1 pr-3">
									<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">HK Alauddin</a>
									<span class="text-muted font-weight-bold font-size-sm my-1">Jln Kebangsaan</span>
								</div>
								<!--end::Title-->
								<!--begin::Info-->
								<div class="d-flex align-items-center py-lg-0 py-2">
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-size-h4 font-weight-bolder">7,200</span>
										<span class="text-muted font-size-sm font-weight-bolder">Work Order & Parts</span>
									</div>
								</div>
								<!--end::Info-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center mb-10">
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
									<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">HK Gowa</a>
									<span class="text-muted font-weight-bold font-size-sm my-1">Jln Kebangsaan</span>
								</div>
								<!--end::Title-->
								<!--begin::Info-->
								<div class="d-flex align-items-center py-lg-0 py-2">
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-size-h4 font-weight-bolder">36,450</span>
										<span class="text-muted font-size-sm font-weight-bolder">Work Order & Parts</span>
									</div>
								</div>
								<!--end::Info-->
							</div>
							<!--end::Item-->
							<!--begin::Item-->
							<div class="d-flex flex-wrap align-items-center">
								<!--begin::Title-->
								<div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
									<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">HK Daya</a>
									<span class="text-muted font-weight-bold font-size-sm my-1">Jln Kebangsaan</span>
								</div>
								<!--end::Title-->
								<!--begin::Info-->
								<div class="d-flex align-items-center py-lg-0 py-2">
									<div class="d-flex flex-column text-right">
										<span class="text-dark-75 font-weight-bolder font-size-h4">23,900</span>
										<span class="text-muted font-size-sm font-weight-bolder">Work Order & Parts</span>
									</div>
								</div>
								<!--end::Info-->
							</div>
							<!--end::Item-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::List Widget 14-->
				</div>
			</div>
			<!--end::Row-->
			<!--begin::Row-->
			<div class="row">
				<div class="col-lg-12 col-xxl-12">
					<!--begin::Advance Table Widget 9-->
					<div class="card card-custom card-stretch gutter-b">
						<!--begin::Header-->
						<div class="card-header border-0 py-5">
							<h3 class="card-title align-items-start flex-column">
								<span class="card-label font-weight-bolder text-dark">Top Mechanic</span>
							</h3>
							<div class="card-toolbar">
								<a href="#" class="btn btn-info font-weight-bolder font-size-sm mr-3">New Arrivals</a>
								<a href="#" class="btn btn-danger font-weight-bolder font-size-sm">Create</a>
							</div>
						</div>
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body pt-0 pb-3">
							<div class="tab-content">
								<!--begin::Table-->
								<div class="table-responsive">
									<table class="table table-head-custom table-vertical-center table-head-bg table-borderless">
										<thead>
											<tr class="text-left">
												<th style="min-width: 250px" class="pl-7">
													<span class="text-dark-75">products</span>
												</th>
												<th style="min-width: 120px">earnings</th>
												<th style="min-width: 100px">comission</th>
												<th style="min-width: 100px">company</th>
												<th style="min-width: 100px">rating</th>
												<th style="min-width: 100px"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="pl-0 py-8">
													<div class="d-flex align-items-center">
														<div class="symbol symbol-50 symbol-light mr-4">
															<span class="symbol-label">
																<img src="<?= base_url() ?>/theme/assets/media/svg/avatars/001-boy.svg" class="h-75 align-self-end" alt="" />
															</span>
														</div>
														<div>
															<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Brad Simmons</a>
															<span class="text-muted font-weight-bold d-block">HTML, JS, ReactJS</span>
														</div>
													</div>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$8,000,000</span>
													<span class="text-muted font-weight-bold">In Proccess</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
													<span class="text-muted font-weight-bold">Paid</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">Intertico</span>
													<span class="text-muted font-weight-bold">Web, UI/UX Design</span>
												</td>
												<td>
													<img src="<?= base_url() ?>/theme/assets/media/logos/stars.png" alt="image" style="width: 5rem" />
													<span class="text-muted font-weight-bold d-block">Best Rated</span>
												</td>
												<td class="pr-0 text-right">
													<a href="#" class="btn btn-light-success font-weight-bolder font-size-sm">View Offer</a>
												</td>
											</tr>
											<tr>
												<td class="pl-0 py-0">
													<div class="d-flex align-items-center">
														<div class="symbol symbol-50 symbol-light mr-4">
															<span class="symbol-label">
																<img src="<?= base_url() ?>/theme/assets/media/svg/avatars/018-girl-9.svg" class="h-75 align-self-end" alt="" />
															</span>
														</div>
														<div>
															<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Jessie Clarcson</a>
															<span class="text-muted font-weight-bold d-block">C#, ASP.NET, MS SQL</span>
														</div>
													</div>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$23,000,000</span>
													<span class="text-muted font-weight-bold">Pending</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$1,600</span>
													<span class="text-muted font-weight-bold">Rejected</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">Agoda</span>
													<span class="text-muted font-weight-bold">Houses &amp; Hotels</span>
												</td>
												<td>
													<img src="<?= base_url() ?>/theme/assets/media/logos/stars.png" alt="image" style="width: 5rem" />
													<span class="text-muted font-weight-bold d-block">Above Average</span>
												</td>
												<td class="pr-0 text-right">
													<a href="#" class="btn btn-light-success font-weight-bolder font-size-sm">View Offer</a>
												</td>
											</tr>
											<tr>
												<td class="pl-0 py-8">
													<div class="d-flex align-items-center">
														<div class="symbol symbol-50 symbol-light mr-4">
															<span class="symbol-label">
																<img src="<?= base_url() ?>/theme/assets/media/svg/avatars/047-girl-25.svg" class="h-75 align-self-end" alt="" />
															</span>
														</div>
														<div>
															<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Lebron Wayde</a>
															<span class="text-muted font-weight-bold d-block">PHP, Laravel, VueJS</span>
														</div>
													</div>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$34,000,000</span>
													<span class="text-muted font-weight-bold">Paid</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$6,700</span>
													<span class="text-muted font-weight-bold">Paid</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">RoadGee</span>
													<span class="text-muted font-weight-bold">Transportation</span>
												</td>
												<td>
													<img src="<?= base_url() ?>/theme/assets/media/logos/stars.png" alt="image" style="width: 5rem" />
													<span class="text-muted font-weight-bold d-block">Best Rated</span>
												</td>
												<td class="pr-0 text-right">
													<a href="#" class="btn btn-light-success font-weight-bolder font-size-sm">View Offer</a>
												</td>
											</tr>
											<tr>
												<td class="pl-0 py-0">
													<div class="d-flex align-items-center">
														<div class="symbol symbol-50 symbol-light mr-4">
															<span class="symbol-label">
																<img src="<?= base_url() ?>/theme/assets/media/svg/avatars/014-girl-7.svg" class="h-75 align-self-end" alt="" />
															</span>
														</div>
														<div>
															<a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Natali Trump</a>
															<span class="text-muted font-weight-bold d-block">Python, PostgreSQL, ReactJS</span>
														</div>
													</div>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$2,600,000</span>
													<span class="text-muted font-weight-bold">Paid</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">$14,000</span>
													<span class="text-muted font-weight-bold">Pending</span>
												</td>
												<td>
													<span class="text-dark-75 font-weight-bolder d-block font-size-lg">The Hill</span>
													<span class="text-muted font-weight-bold">Insurance</span>
												</td>
												<td>
													<img src="<?= base_url() ?>/theme/assets/media/logos/stars.png" alt="image" style="width: 5rem" />
													<span class="text-muted font-weight-bold d-block">Average</span>
												</td>
												<td class="pr-0 text-right">
													<a href="#" class="btn btn-light-success font-weight-bolder font-size-sm">View Offer</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!--end::Table-->
							</div>
						</div>
						<!--end::Body-->
					</div>
					<!--end::Advance Table Widget 9-->
				</div>
			</div>
			<!--end::Row-->
			<!--end::Dashboard-->
		</div>
		<!--end::Container-->
	</div>
	<!--end::Entry-->
</div>
<?= $this->endSection() ?>
<!--begin::Page Scripts(used by this page)-->
<?= $this->section('extra-js') ?>
<script src="<?= base_url() ?>/theme/assets/js/pages/widgets.js"></script>
<script src="<?= base_url() ?>/theme/assets/js/pages/features/charts/apexcharts.js"></script>
<?= $this->endSection() ?>
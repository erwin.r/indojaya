<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use Config\Exceptions;

class TimBiner extends Controller
{
   use ResponseTrait;

   public function __construct()
   {
      helper('inflector');
      $this->validation = \Config\Services::validation();
      $this->db = \Config\Database::connect();
      $this->table = new \CodeIgniter\View\Table();
   }

   /**
    * Halaman Generator
    *
    * Baca instruksi yang terdapat di bagian header.
    * Konten sisi kiri memuat form generator.
    * Konten sisi kiri memuat log aktifitas generator
    */
   public function index()
   {
      try {
         $tables = $this->db->listTables();
         $data = [
            'tables' => $tables,
            'content' => 'timbiner/content/home'
         ];
      } catch (\Exception $e) {
         $data = [
            'message' => $e->getMessage(),
            'content' => 'timbiner/content/error'
         ];
      }
      return view('timbiner/main', $data);
   }

   /**
    * Proses Generator
    * by VektorLutfi
    * Proses pembuatan model, view, dan controller.
    * Terdapat validasi umum terhadap field-field yang ada.
    * Terdapat validasi untuk memilih tabel yang dimaksud
    * jika terdapat relasi antara keduanya. Terdapat validasi
    * format penamaan model dan controller
    */
   public function generate()
   {
      $request = $this->request->getPost();
      $this->rules();
      $data = [];
      if ($this->validation->run($request) == FALSE) {

         $message = $this->validation->getErrors();
         return $this->fail($message);
      } else {
         $controllerName = $request['controller'];
         $modelName = $request['model'];

         if ($this->position($controllerName) == false || $this->position($modelName) == false) {

            if ($this->position($controllerName) == false)
               $message['controller'] = 'The Controller field must contain the word "TimBiner"';
            if ($this->position($modelName) == false)
               $message['model'] = 'The Model field must contain the word "TimBiner"';

            return $this->fail($message);
         } else {
            // pecah data table
            foreach ($request['table'] as $tables => $table) {
               $format = ['TimBiner'];
               $controllerName = $this->replace($request['controller'], $table);
               $otherControllerName = '';
               $otherModelName = [];

               $fieldData = $this->db->getFieldData($table);
               $foreignKeyData = $this->db->getForeignKeyData($table);

               $nonPk = [];

               // pecah kolom
               foreach ($fieldData as $col) {
                  $col->primary_key == 1 ? $pk = $col : $nonPk[] = $col;
               }
               // .pecah kolom

               // pecah non pk
               $groupNonPkName = [];
               foreach ($nonPk as $cols => $col) {
                  if ($col->type == 'enum') {
                     $getRow = $this->db->query("SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS
                     WHERE TABLE_NAME = '$table' AND COLUMN_NAME = '$col->name'")->getRowArray();

                     $enumList = explode(",", str_replace("'", "", substr($getRow['COLUMN_TYPE'], 5, (strlen($getRow['COLUMN_TYPE']) - 6))));


                     foreach ($enumList as $enum) {
                        $col->option_value[] = $enum;
                     }
                  }
                  // grup nama non pk
                  // if ($col->type == 'tinyblob' || $col->type == 'blob' || $col->type == 'mediumblob' || $col->type == 'longblob') {
                  $groupNonPkName[] = $col->name;
                  // }
                  // .grup nama non pk
               }
               // .pecah non pk

               // cek kolom mime type jika terdapat tipe data blob
               foreach ($nonPk as $cols => $col) {
                  if ($col->type == 'tinyblob' || $col->type == 'blob' || $col->type == 'mediumblob' || $col->type == 'longblob') {
                     if (!in_array($col->name . '_media_type', $groupNonPkName)) {
                        $message[$col->name] = 'The "' . $col->name . '" column was detected as a BLOB data type. An additional "' . $col->name . '_media_type" column in the "' . $table . '" table is required to store Mime Type data from a file.';
                        return $this->fail($message);
                     }
                  }
               }
               // .cek kolom mime type jika terdapat tipe data blob

               // cek fk
               if (!empty($foreignKeyData)) {
                  // pecah data fk
                  foreach ($foreignKeyData as $fks => $fk) {
                     if (!in_array($fk->foreign_table_name, $request['table'])) {

                        $message[$fk->foreign_table_name] = 'The "' . $fk->foreign_table_name . '" table is required because the "' . $fk->column_name . '" column in the "' . $fk->table_name . '" table is related to the "' . $fk->foreign_column_name . '" column in the "' . $fk->foreign_table_name . '" table.';

                        return $this->fail($message);
                     } else {
                        $otherControllerName = $this->replace($request['controller'], $fk->foreign_table_name);
                        $otherModelName[] = $this->replace($request['model'], $fk->foreign_table_name);
                        $modelName = $this->replace($request['model'], $table);
                     }
                  }
                  // .pecah data fk
               } else {
                  $modelName = $this->replace($request['model'], $table);
               }
               // .cek fk

               $data[] = [
                  'pk' => $pk,
                  'non_pk' => $nonPk,
                  'fk' => $foreignKeyData,
                  'table' => $table,
                  'controller' => $controllerName,
                  'model' => $modelName,
                  'other_controller' => $otherControllerName,
                  'other_model' => $otherModelName
               ];
            }

            if (!is_dir(APPPATH . './../Modules/' . $controllerName)) {
               mkdir(APPPATH . "./../Modules/" . $controllerName);
            }

            // .pecah data table
            $this->GenerateController($data);
            $this->GenerateModel($data);
            $this->GenerateView($data);

            $message = 'Success! generated successfully';

            return $this->respond($message);
         }
      }
   }

   /**
    * Aturan Generator
    * by VektorLutfi
    * Aturan-aturan yang berlaku saat melakukan generate
    * yatu field table, field model, dan field controller.
    */
   private function rules()
   {
      $this->validation->setRules([
         'table' =>
         [
            'label'  => 'Table',
            'rules'  => 'required'
         ],
         'model' =>
         [
            'label'  => 'Model',
            'rules'  => 'required|regex_match[/^[A-Za-z_]+$/]'
         ],
         'controller' => [
            'label'  => 'Controller',
            'rules'  => 'required|regex_match[/^[A-Za-z_]+$/]'
         ],
      ]);
   }

   /**
    * Format Penamaan Controller dan Model
    * by VektorLutfi
    * Pada nama controller dan nama model wajib
    * mencantumkan kata "TimBiner". Secara bawaan nama model
    * adalah "TimBiner", dan nama model yaitu "TimBinerModel".
    * Penambahan prefix yang bersifat opsional.
    * @access private
    * @param string
    * @return bool
    */
   private function position($string)
   {
      $format = 'TimBiner';

      if (strpos($string, $format) !== false) {
         return true;
      } else {
         return false;
      }
   }

   /**
    * Timpa Kata TimBiner Menjadi Nama Tabel
    *
    * Hal ini bertujuan untuk mengidentifikasi penamaan
    * controller dan model
    * @access private
    * @param string
    * @return string
    */
   private function replace($string, $table)
   {
      $format = 'TimBiner';

      return str_replace($format, pascalize($table), $string);
   }

   /**
    * Membuat Controller
    *
    * Terdapat aturan validasi request.
    * Terdapat method CRUD. Response berupa
    * data json dan html.
    * @access public
    * @param array
    */
   public function GenerateController($request = [])
   {
      foreach ($request as $datas => $data) {
         $controllerTemplate = '<?php
namespace Modules\\' . $data['controller'] . '\\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;';

         $controllerTemplate .= '
use Modules\\' . $data['controller'] . '\Models\\' . $data['model'] . ';';

         if (!empty($data['other_model'])) {
            foreach ($data['other_model'] as $otherModel) {
               $controllerTemplate .= '
use Modules\\' . substr(trim($otherModel), 0, -5) . '\Models\\' . $otherModel . ';';
            }
         }


         $controllerTemplate .= '
class ' . $data['controller'] . ' extends BaseController{
  use ResponseTrait;

  protected $' . $data['model'] . ';
  protected $edit;
  protected $delete;

  public function __construct() {';
         $controllerTemplate .= '
      $this->validation = \Config\Services::validation();
      $this->' . $data['model'] . ' = new ' . $data['model'] . ';';

         if (!empty($data['other_model'])) {
            foreach ($data['other_model'] as $otherModel) {
               $controllerTemplate .= '
      $this->' . $otherModel . ' = new ' . $otherModel . ';';
            }
         }
         $controllerTemplate .= '
      $this->session = session();
      $this->edit = 0;
      $this->delete = 0;';

         $controllerTemplate .= '

  }

  function index(){
     $data = [
        \'title\' => \'Data ' . humanize($data['table']) . '\',
        \'host\' => site_url(\'' . strtolower($data['controller']) . '/\')
     ];
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     if($menu){
        $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
        if(!$this->ionAuth->hasAccess($menu->id,$groupId))
         echo view(\'errors\html\error_404\'); 
        else{
         $data = [
           \'title\' => \'Data ' . humanize($data['table']) . '\',
           \'host\' => site_url(\'' . strtolower($data['controller']) . '/\'),
           \'add\' => $this->ionAuth->permissionAction($menu->id,\'add\',$groupId),
         ];
         echo view(\'Modules\\' . $data['controller'] . '\Views\list\', $data);
        }
     } else echo view(\'errors\html\error_404\'); 
  }

  public function data(){
      $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
      $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
      $this->edit = $this->ionAuth->permissionAction($menu->id,\'edit\',$groupId);
      $this->delete = $this->ionAuth->permissionAction($menu->id,\'delete\',$groupId);

      $where = "1=1";
      $request = esc($this->request->getPost());';
         $sf = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               if ($sf < 6)
                  $controllerTemplate .= '
      $' . $nonPk->name . ' = $request[\'' . $nonPk->name . '\'];';
               $sf++;
            }
         }

         $controllerTemplate .= '
      
      $cols = array();';
         $sf = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               if ($sf < 6)
                  $controllerTemplate .= '
      if (!empty($' . $nonPk->name . ')) { $cols[\'' . $nonPk->name . '\'] = $' . $nonPk->name . '; }';
               $sf++;
            }
         }

         $controllerTemplate .= '
      
      $search = $request[\'search\'][\'value\'];
      $limit = $request[\'length\'];
      $start = $request[\'start\'];

      $orderIndex = $request[\'order\'][0][\'column\'];
      $orderFields = $request[\'columns\'][$orderIndex][\'data\'];
      $orderDir = $request[\'order\'][0][\'dir\'];

      $list = $this->' . $data['model'] . '->filter($cols, $search, $limit, $start, $orderFields, $orderDir,$where)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = \'\';
            
            $btn_actions .= \'
                           <div class="dropdown dropdown-inline mr-4 dropright">
                              <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="ki ki-bold-more-hor"></i>
                              </button>
                              <div class="dropdown-menu">
                                 <ul class="navi navi-hover">\';
            $btn_actions .= \'
                                    <li class="navi-item">
                                       <a class="navi-link btn-edit" data-id="\'.$r->' . $data['pk']->name . '.\'">
                                          <span class="navi-icon">
                                             <i class="fas fa-pencil-alt text-warning"></i>
                                          </span>
                                          <span class="navi-text">Edit</span>
                                       </a>
                                    </li>\';
            $btn_actions .= \'
                                    <li class="navi-item">
                                       <a class="navi-link btn-delete" data-id="\'.$r->' . $data['pk']->name . '.\'">
                                          <span class="navi-icon">
                                             <i class="fas fa-trash text-danger"></i>
                                          </span>
                                          <span class="navi-text">Delete</span>
                                       </a>
                                    </li>\';
            $btn_actions .= \'
                                 </ul>
                              </div>
                           </div>\';
            
            $row[] = $btn_actions;
            $row[] = $no;';

         $sf = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               if ($sf < 6)
                  $controllerTemplate .= '
            $row[] = $r->' . $nonPk->name . ';';
               $sf++;
            }
         }

         $controllerTemplate .= '
            $data[] = $row;

      }

      return $this->respond([
         \'draw\'            => $this->request->getPost(\'draw\'),
         \'recordsTotal\'    => $this->' . $data['model'] . '->countTotal($where),
         \'recordsFiltered\' => $this->' . $data['model'] . '->countFilter($cols, $search, $where),
         \'data\'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [';
         if (!empty($data['other_model'])) {
            $om = 0;
            foreach ($data['other_model'] as $otherModel) {
               $controllerTemplate .= '
        \'data_' . $data['fk'][$om]->foreign_table_name . '\' => $this->' . $otherModel . '->findAll(),';
               $om++;
            }
         }

         $controllerTemplate .= '
   ];
   return view(\'Modules\\' . $data['controller'] . '\Views\add\', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            \'status\' => 400,
            \'error\' => 400,
            \'messages\' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [';
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               $controllerTemplate .= '
               \'' . $nonPk->name . '\' => $this->request->getPost(\'' . $nonPk->name . '\'),';
            }
         }

         $controllerTemplate .= '
              \'user_created\' => $this->session->get(\'user_id\'),';

         $controllerTemplate .= '
         ];

         if ($this->' . $data['model'] . '->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->' . $data['model'] . '->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet(\'id\');
      $data = [';
         if (!empty($data['other_model'])) {
            $om = 0;
            foreach ($data['other_model'] as $otherModel) {
               $controllerTemplate .= '
        \'data_' . $data['fk'][$om]->foreign_table_name . '\' => $this->' . $otherModel . '->findAll(),';
               $om++;
            }
         }
         $controllerTemplate .= '
      ];
      $data[\'main\'] = $this->' . $data['model'] . '->find($id);
      return view(\'Modules\\' . $data['controller'] . '\Views\edit\', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost(\'id\');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            \'status\' => 400,
            \'error\' => 400,
            \'messages\' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
   ';
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               $controllerTemplate .= '
               \'' . $nonPk->name . '\' => $this->request->getPost(\'' . $nonPk->name . '\'),';
            }
         }
         $controllerTemplate .= '
              \'user_update\' => $this->session->get(\'user_id\'),';

         $controllerTemplate .= '
         ];
         if ($this->' . $data['model'] . '->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->' . $data['model'] . '->errors());
      }
    }

    public function delete($id)
    {
        if ($found = $this->' . $data['model'] . '->where(\'' . $data['pk']->name . '\', $id)->delete()) {
            return $this->respondDeleted($found);
        }
        return $this->fail(\'Fail deleted\');
    }

    private function rules(){
      $this->validation->setRules([';
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               $controllerTemplate .= '
         \'' . $nonPk->name . '\' => [
            \'label\' => \'' . humanize($nonPk->name) . '\',
            \'rules\' => \'';

               if ($nonPk->type == 'tinyint' || $nonPk->type == 'smallint' || $nonPk->type == 'mediumint' || $nonPk->type == 'int' || $nonPk->type == 'bigint') {
                  $controllerTemplate .= 'required|numeric';
               } else if ($nonPk->type == 'float' || $nonPk->type == 'double' || $nonPk->type == 'real' || $nonPk->type == 'decimal' || $nonPk->type == 'numeric') {
                  $controllerTemplate .= 'required|decimal';
               } else if ($nonPk->type == 'time') {
                  $controllerTemplate .= 'required|valid_date[H:i]';
               } else if ($nonPk->type == 'date') {
                  $controllerTemplate .= 'required|valid_date[Y-m-d]';
               } else if ($nonPk->type == 'year') {
                  $controllerTemplate .= 'required|valid_date[Y]';
               } else if ($nonPk->type == 'datetime') {
                  $controllerTemplate .= 'required|valid_date[Y-m-d H:i]';
               } else if ($nonPk->type == 'enum') {
                  $controllerTemplate .= 'required|in_list[';
                  $e = 1;
                  foreach ($nonPk->option_value as $enum) {
                     $controllerTemplate .= $enum;
                     if ($e < count($nonPk->option_value)) {
                        $controllerTemplate .= ', ';
                     }
                     $e++;
                  }
                  $controllerTemplate .= ']';
               } else if ($nonPk->type == 'tinyblob' || $nonPk->type == 'blob' || $nonPk->type == 'mediumblob' || $nonPk->type == 'longblob') {
                  $controllerTemplate .= 'ext_in[' . $nonPk->name . ',png,jpg,pdf]|mime_in[' . $nonPk->name . ',image/png,image/jpeg,application/pdf]|max_size[' . $nonPk->name . ',2048]';
               } else {
                  $controllerTemplate .= 'required|string';
               }

               if ($nonPk->max_length != null) {
                  $controllerTemplate .= '|max_length[' . $nonPk->max_length . ']';
               }


               $controllerTemplate .= '\'
         ],';
            }
         }


         $controllerTemplate .= '
      ]);
   }

}';

         // make dir
         if (!is_dir(APPPATH . './../Modules/' . $data['controller'])) {
            mkdir(APPPATH . './../Modules/' . $data['controller']);
         }

         // make dir
         if (!is_dir(APPPATH . './../Modules/' . $data['controller'] . '/Config')) {
            mkdir(APPPATH . './../Modules/' . $data['controller'] . '/Config');
         }

         if (!is_dir(APPPATH . './../Modules/' . $data['controller'] . '/Controllers')) {
            mkdir(APPPATH . './../Modules/' . $data['controller'] . '/Controllers');
         }

         // make controller
         $controllerFile = fopen(APPPATH . './../Modules/' . $data['controller'] . '/Controllers/' . $data['controller'] . '.php', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($controllerFile, $controllerTemplate);
         fclose($controllerFile);
         // .make controller

         // make Router
         $routerTemplate = '<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group(\'' . strtolower($data['controller']) . '\', [\'namespace\' => \'Modules\\' . $data['controller'] . '\Controllers\',\'filter\' => \'auth\'], function($routes)
      {
          $routes->get(\'/\', \'' . $data['controller'] . '::index\');
          $routes->get(\'edit\', \'' . $data['controller'] . '::load_form_edit\');
          $routes->get(\'add\', \'' . $data['controller'] . '::load_form_add\');
          $routes->post(\'updated\', \'' . $data['controller'] . '::updated\');
          $routes->post(\'addnew\', \'' . $data['controller'] . '::new\');
          $routes->post(\'data\', \'' . $data['controller'] . '::data\');
          $routes->resource(\'resources\', [
              \'controller\' => \'' . $data['controller'] . '\',
          ]);
      });
      ';

         // make controller
         $routerFile = fopen(APPPATH . './../Modules/' . $data['controller'] . '/Config/Routes.php', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($routerFile, $routerTemplate);
         fclose($routerFile);
         // .make controller
      }
   }

   /**
    * Membuat Model
    *
    * Terdapat file model sesuai banyaknya tabel.
    * Terdapat inisialisasi nama table, primary key,
    * field-field yang diperbolehkan untuk diisi dan
    * field-field yang diperbolehkan untuk pencarian..
    * Terdapat beberapa method yang digunakan untuk
    * menampilkan data dari permintaan datatable.
    * @access public
    * @param array
    */
   public function GenerateModel($request = [])
   {
      foreach ($request as $datas => $data) {
         $modelTemplate = '<?php
namespace Modules\\' . $data['controller'] . '\\Models;
use CodeIgniter\Model;

class ' . $data['model'] . ' extends Model{
   protected $table      = \'' . $data['table'] . '\';';

         $modelTemplate .= '
   protected $primaryKey = \'' . $data['pk']->name . '\';
   // if use uuid set to false
   protected $useAutoIncrement = false;
   protected $useSoftDeletes = true;
   protected $useTimestamps = true;
   protected $createdField  = \'created_at\';
   protected $updatedField  = \'updated_at\';
   protected $deletedField  = \'deleted_at\';';

         $modelTemplate .= '
   protected $allowedFields = [';
         // pecah non pk
         $af = 1;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            $modelTemplate .= '\'' . $nonPk->name . '\'';
            if ($af < count($data['non_pk'])) {
               $modelTemplate .= ', ';
            }
            $af++;
         }
         // .pecah non pk
         $modelTemplate .= '];';

         $modelTemplate .= '
   protected $searchFields = [';
         // pecah non pk
         $sf = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($sf < 5)
               $modelTemplate .= '\'' . $nonPk->name . '\'';
            if ($sf < 4)
               $modelTemplate .= ', ';

            $sf++;
         }
         // .pecah non pk
         $modelTemplate .= '];';


         $modelTemplate .= '

   public function filter($cols = null, $search = null, $limit = null, $start = null, $orderField = null, $orderDir = null, $where = null){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if(!empty($cols)){
         foreach ($cols as $col => $value)
         {
            $builder->like($col, $value);
         }
      }

      // Secara bawaan menampilkan data sebanyak kurang dari
      // atau sama dengan 7 kolom pertama.
      $builder->select(\'' . $data['table'] . '.' . $data['pk']->name . ', ';

         $qr = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($qr < 6)
               $modelTemplate .= $data['table'] . '.' . $nonPk->name;
            if ($qr < 5)
               $modelTemplate .= ', ';
            $qr++;
         }

         $modelTemplate .= '\')';

         if (!empty($data['fk'])) {
            foreach ($data['fk'] as $fks => $fk) {
               $modelTemplate .= '
              ->join(\'' . $fk->foreign_table_name . '\', \'' . $fk->foreign_table_name . '.' . $fk->foreign_column_name . ' = ' . $fk->table_name . '.' . $fk->column_name . '\')';
            }
         }

         $modelTemplate .= '
              ->where(\'' . $data['table'] . '.deleted_at is NULL\')
              ->where($where)
              ->orderBy(\'' . $data['table'] . '.created_at\', \'DESC\')
              ->orderBy($orderField, $orderDir)
              ->limit($limit, $start);

      $query = $builder;

      return $query;
   }

   public function countTotal($where){
      return $this->table($this->table)->where($where)';

         if (!empty($data['fk'])) {
            foreach ($data['fk'] as $fks => $fk) {
               $modelTemplate .= '
                  ->join(\'' . $fk->foreign_table_name . '\', \'' . $fk->foreign_table_name . '.' . $fk->foreign_column_name . ' = ' . $fk->table_name . '.' . $fk->column_name . '\')';
            }
         }

         $modelTemplate .= '
                  ->countAllResults();
   }

   public function countFilter($cols, $search, $where){
      $builder = $this->table($this->table)->where($where);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if (!empty($cols)) {
        foreach ($cols as $col => $value) {
          $builder->like($col, $value);
        }
      }

      return $builder';

         if (!empty($data['fk'])) {
            foreach ($data['fk'] as $fks => $fk) {
               $modelTemplate .= '->join(\'' . $fk->foreign_table_name . '\', \'' . $fk->foreign_table_name . '.' . $fk->foreign_column_name . ' = ' . $fk->table_name . '.' . $fk->column_name . '\')
                     ';
            }
         }

         $modelTemplate .= '->countAllResults();
   }

}';

         // make dir
         if (!is_dir(APPPATH . './../Modules/' . $data['controller'] . '/Models/')) {
            mkdir(APPPATH . './../Modules/' . $data['controller'] . '/Models/');
         }

         // make model
         $formFile = fopen(APPPATH . './../Modules/' . $data['controller'] . '/Models/' . $data['model'] . '.php', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($formFile, $modelTemplate);
         fclose($formFile);
         // .make model
      }
   }

   /**
    * Membuat View
    *
    * Terdapat tiga file view yaitu list.php, add.php dan edit.php.
    * List.php digunakan untuk melakukan perintah CRUD
    * add.php digunakan untuk menambah data,
    * edit.php digunakan untuk mengedit data,
    * mendukung beberapa tipe field seperti input, textarea, radio dan combobox.
    * @access public
    * @param array
    */
   public function GenerateView($request = [])
   {
      foreach ($request as $datas => $data) {

         $listTemplate = '
<?= $this->extend(\'layout/backend/main\') ?>
<?= $this->section(\'content\') ?>
   <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-5 py-lg-10 gutter-b subheader-transparent" id="kt_subheader" style="background-color: #266b31; background-position: right bottom; background-size: auto 100%; background-repeat: no-repeat; background-image: url(<?php echo base_url(); ?>/theme/assets/media/svg/patterns/taieri.svg)">
            <div class="container d-flex flex-column">
                <div class="d-flex align-items-sm-end flex-column flex-sm-row mb-5">
                    <h2 class="d-flex align-items-center text-white mr-5 mb-0">Search</h2>
                    <span class="text-white opacity-60 font-weight-bold">Module ' . $data['controller'] . ' Management</span>
                    </div>
                <div class="d-flex align-items-md-center mb-2 flex-column flex-md-row">
                    <div class="bg-white rounded p-4 d-flex flex-grow-1 flex-sm-grow-0">
                        <form id="form-filter" class="form d-flex align-items-md-center flex-sm-row flex-column flex-grow-1 flex-sm-grow-0">';
         $i = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               if ($i < 6) {
                  $listTemplate .= '
                            <div class="d-flex align-items-center py-3 py-sm-0 px-sm-3">
                                <span class="svg-icon svg-icon-lg">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>
                                </span>
                                <input type="text" id="' . $nonPk->name . '" class="form-control border-0 font-weight-bold pl-2" placeholder="' . humanize($nonPk->name) . '">
                            </div>';
               }
               $i++;
            }
         }
         $listTemplate .= '
                            <button type="button" id="kt_search" class="btn btn-dark font-weight-bold btn-hover-light-primary mt-3 mt-sm-0 px-7">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-delivery-package text-primary"></i>
                            </span>
                            <h3 class="card-label">Module ' . $data['controller'] . '</h3>
                        </div>                                    
                          <div class="card-toolbar">                          
                              <a id="create" class="btn btn-primary font-weight-bolder">New Record</a>                            
                          </div>                        
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin: Datatable-->
                        <table class="table table-bordered table-hover table-checkable" id="data-table-book">
                            <thead>
                                <tr role="row">
                                    <th width="1%">Action</th>
                                    <th width="2%">No</th>';
         $i = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {

               if ($i < 6) {
                  $listTemplate .= '
                                      <th>' . humanize($nonPk->name) . '</th>';
               }
               $i++;
            }
         }
         $listTemplate .= '
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
         
    <!-- /.row -->
<?= $this->endSection() ?>

<!--begin::Page Vendors Styles(used by this page)-->
<?= $this->section(\'page-vendors-styles-js\') ?>
<link href="<?= base_url() ?>/theme/backend/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?= $this->endSection() ?>

<!--begin::Page Vendors(used by this page)-->
<?= $this->section(\'page-vendors-js\') ?>
<script src="<?= base_url() ?>/theme/backend/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<?= $this->endSection() ?>

<!--begin::Page Scripts(used by this page)-->
<?= $this->section(\'extra-js\') ?>
<!-- <script src="/theme/assets/js/pages/crud/datatables/search-options/advanced-search.js"></script> -->
<script src="<?= base_url() ?>/theme/backend/js/' . $data['controller'] . '/' . $data['controller'] . '.js"></script>
<script>Page.init();</script>
<?= $this->endSection() ?>

';

         // make dir
         if (!is_dir(APPPATH . './../Modules/' . $data['controller'] . '/Views/')) {
            mkdir(APPPATH . './../Modules/' . $data['controller'] . '/Views/');
         }

         // make dir
         if (!is_dir(APPPATH . './../public/theme/backend/js/' . $data['controller'])) {
            mkdir(APPPATH . './../public/theme/backend/js/' . $data['controller']);
         }

         // make list
         $listFile = fopen(APPPATH . './../Modules/' . $data['controller'] . '/Views/list.php', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($listFile, $listTemplate);
         fclose($listFile);
         // .make list


         // .make JS
         $JSTemplate = '
var loadData = base_url + "/' . strtolower($data['controller']) . '/data",
tableID = $("#data-table-book"),
Page = function () {
      return {
         init: function () {
            Page.main();
            Page.tableAjax();
         },
         tableAjax: function () {
            $.ajaxSetup({
                  headers: {
                     \'X-CSRF-TOKEN\': $(\'meta[name="X-CSRF-TOKEN"]\').attr(\'content\')
                  }
            });

            //datatables
            tableID.DataTable({
                  lengthMenu: [10, 20, 50, 100],
                  pageLength: 10,
                  language: {
                     \'lengthMenu\': \'Display _MENU_\',
                  },
                  searchDelay: 500,
                  responsive: true,
                  autoWidth: false,
                  serverSide : true,
                  processing: true,
                  searching: false,
                  order: [[1, \'asc\']],
                  columnDefs: [{
                     orderable: false,
                     targets: [0]
                  },{ responsivePriority: 3, targets: -1 }],

                  ajax : {
                     url: loadData,
                     method : \'POST\',
                     data: function (d) {
                        // get value for search';
         $i = 0;
         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               if ($i < 6) {
                  $JSTemplate .= '
                        d.' . $nonPk->name . ' = $(\'#' . $nonPk->name . '\').val();';
               }
               $i++;
            }
         }
         $JSTemplate .= '
                        
                    }
                  }

            });

            $(\'#kt_search\').click(function () { //button filter event click
                  tableID.DataTable().draw(); //just reload table
            });

         },';

         $JSTemplate .= '
         main: function () {
            $("#modal-create-book").on("hidden.bs.modal", function() {
                  $(this).find("#form-create-book")[0].reset();
                  $(".text-danger").remove();
                  $(".is-invalid").removeClass("is-invalid");
            });

            $(document).on("click", "#create", function () {
                  Page.add()
            });

            $(document).on("submit", "#form-add", function () {
                  return Page.submitForm($("#form-add"), base_url + "/' . strtolower($data['controller']) . '/addnew"), !1
            });

            $(document).on("click", ".btn-edit", function () {
                  var e = $(this).attr("data-id");
                  Page.edit(e)
            });

            $(document).on("submit", "#form-edit", function () {
                  var id = $("#book_id").val();
                  return Page.submitForm($("#form-edit"), base_url + "/' . strtolower($data['controller']) . '/updated"), !1
            });

            $(document).on("click", ".btn-delete", function (e) {
                  Swal.fire({
                     title: "Are you sure?",
                     text: "You won\'t be able to revert this!",
                     icon: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#3085d6",
                     cancelButtonColor: "#d33",
                     confirmButtonText: "Yes, delete it!"
                  })
                  .then((result) => {
                     if (result.value) {
                        $.ajax({
                              url: base_url + "/' . strtolower($data['controller']) . '/resources/"+$(this).attr("data-id"),
                              method: "DELETE",
                        }).done((data, textStatus) => {
                              swal.fire({
                                 icon: "success",
                                 title: textStatus,
                              });
                              tableID.DataTable().ajax.reload();
                        }).fail((error) => {
                              swal.fire({
                                 icon: "error",
                                 title: error.responseJSON.messages.error,
                              });
                        })
                     }
                  })
            });
         },
         add: function () {
            var a = Helper.loadModal("lg"),
                  i = a.find(".modal-body"),
                  t = a.find(".modal-title");
            t.text("Add Data"), 
            Helper.blockElement($(i)), 
            $.ajax({
                  url: base_url + "/' . strtolower($data['controller']) . '/add",
                  method: "GET",
            })
            .done(function (e) {
                  i.html(e);
                  Helper.unblockElement($(i));
            })
         },
         edit: function (e) {
            var a = Helper.loadModal("lg"),
                  i = a.find(".modal-body"),
                  t = a.find(".modal-title");
            t.text("Edit Data"), 
            Helper.blockElement($(i)), 
            $.ajax({
                  url: base_url + "/' . strtolower($data['controller']) . '/edit",
                  method: "GET",
                  data: {id:e}, 
            })
            .done(function (e) {
                  i.html(e);
                  Helper.unblockElement($(i));
            })
         },
         submitForm: function (e, url) {
            var i = e.find(".submit");
            Helper.blockElement(e.parent()), i.attr("disabled", !0);
            $.ajax({
                  url: url,
                  headers: {
                        \'X-CSRF-TOKEN\': $(\'meta[name="X-CSRF-TOKEN"]\').attr(\'content\')
                  },
                  type: "POST", //form method
                  data: e.serialize()
            }).done((data, textStatus) => {
                  swal.fire({
                     icon: "success",
                     title: textStatus
                  })
                  $(".closed").click();
                  $(tableID).DataTable().ajax.reload();
                  e.trigger("reset");
                  Helper.blockElement(e.parent()), i.removeAttr("disabled", !0);
            }).fail((xhr, status, error) => {
                  Helper.unblockElement(e.parent());
                  $.each(xhr.responseJSON.messages, function (key, value) {
                     $("input[name=" + key + "]").addClass(\'is-invalid\');
                     $("." + key).text(value);
                     Helper.removeValidate(key);
                  });
                  i.removeAttr("disabled");
            });
         }
      }
}();';

         // make JS
         $JSFile = fopen(APPPATH . './../public/theme/backend/js/' . $data['controller'] . '/' . $data['controller'] . '.js', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($JSFile, $JSTemplate);
         fclose($JSFile);
         // .make JS

         // Generete Form
         $formTemplate = '<form id="form-add" accept-charset="utf-8">';

         // grup kolom fk
         $groupFk = [];
         foreach ($data['fk'] as $fks => $fk) {
            $groupFk[] = $fk->column_name;
         }
         // .grup kolom fk

         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {
               $formTemplate .= '
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">';

               $formTemplate .= '
            <label for="' . $nonPk->name . '">' . humanize($nonPk->name) . ' <span class="text-danger">*</span></label>';

               // kondisi ada relasi
               // if (!empty($data['fk'])) {
               if (in_array($nonPk->name, $groupFk)) {
                  // pecah fk
                  foreach ($data['fk'] as $fks => $fk) {
                     if ($nonPk->name == $fk->column_name)
                        // type array (combobox)
                        $formTemplate .= '
            <select name="' . $nonPk->name . '" class="custom-select">
               <?php foreach($data_' . $fk->foreign_table_name . ' as $' . plural($fk->foreign_table_name) . ' => $row): ?>
               <option value="<?= $row[\'' . $fk->foreign_column_name . '\'] ?>"><?= $row[\'' . $fk->foreign_column_name . '\'] ?></option>
               <?php endforeach ?>
            </select>';
                     // .type array (combobox)

                  }
                  // .pecah fk
               }
               // .kondisi ada relasi

               // kondisi tidak ada relasi
               else {
                  // type text (textarea)
                  if ($nonPk->type == 'tinytext' || $nonPk->type == 'text' || $nonPk->type == 'mediumtext' || $nonPk->type == 'longtext') {
                     $formTemplate .= '
            <textarea type="text" name="' . $nonPk->name . '" class="form-control" ></textarea>';
                  }
                  // .type text (textarea)

                  // type enum (radio)
                  else if ($nonPk->type == 'enum' || $nonPk->type == 'set') {
                     $radio = 1;
                     foreach ($nonPk->option_value as $enum) {
                        $formTemplate .= '
            <div class="form-check">
               <input class="form-check-input" type="radio" id="' . $nonPk->name . $radio . '" name="' . $nonPk->name . '" />
               <label class="form-check-label" for="' . $nonPk->name . $radio . '">' . humanize($enum) . '</label>
            </div>';
                        $radio++;
                     }
                  }
                  // .type enum (radio)

                  // type time (time)
                  else if ($nonPk->type == 'time') {
                     $formTemplate .= '
            <input type="text" name="' . $nonPk->name . '" class="form-control time" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type time (time)

                  // type date (date)
                  else if ($nonPk->type == 'date') {
                     $formTemplate .= '
            <input type="text" name="' . $nonPk->name . '" class="datepicker form-control date" id="' . $nonPk->name . '" data-date-format="yyyy-mm-dd" readonly />';
                  }
                  // .type date (date)

                  // type datetime (datetime)
                  else if ($nonPk->type == 'datetime') {
                     $formTemplate .= '
            <input type="text" name="' . $nonPk->name . '" class="form-control datetime" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type datetime (datetime)

                  // type year (year)
                  else if ($nonPk->type == 'year') {
                     $formTemplate .= '
            <input type="text" name="' . $nonPk->name . '" class="form-control year" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type year (year)

                  // type blob (file)
                  else if ($nonPk->type == 'tinyblob' || $nonPk->type == 'blob' || $nonPk->type == 'mediumblob' || $nonPk->type == 'longblob') {
                     $formTemplate .= '
            <input type="file"  name="' . $nonPk->name . '" class="form-control" id="file_' . $nonPk->name . '">';
                  }
                  // .type blob (file)

                  // type string (text)
                  else {
                     $formTemplate .= '
            <input type="text" name="' . $nonPk->name . '" class="form-control" />';
                  }
                  // .type string (text)
               }
               // .kondisi tidak ada relasi
               $formTemplate .= '
            <div class="invalid-feedback ' . $nonPk->name . '"></div>
         </div>
      </div>
   </div>';
            }
         }

         $formTemplate .= '
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>';

         // make form
         $formFile = fopen(APPPATH . './../Modules/' . $data['controller'] . '/Views/add.php', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($formFile, $formTemplate);
         fclose($formFile);
         // .make form




         $formTemplateEdit = '<form id="form-edit" accept-charset="utf-8">
    <input type="hidden" value="<?php echo $main[\'' . $data['pk']->name . '\']; ?>" name="id" class="form-control">';

         // grup kolom fk
         $groupFk = [];
         foreach ($data['fk'] as $fks => $fk) {
            $groupFk[] = $fk->column_name;
         }
         // .grup kolom fk

         foreach ($data['non_pk'] as $nonPks => $nonPk) {
            if ($nonPk->name != 'created_at' and $nonPk->name != 'user_created' and $nonPk->name != 'updated_at' and $nonPk->name != 'user_update' and $nonPk->name != 'deleted_at' and $nonPk->name != 'GCRecord' and $nonPk->name != 'OptimisticLockField' and $nonPk->name != 'rowguid') {

               $formTemplateEdit .= '
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">';

               $formTemplateEdit .= '
            <label for="' . $nonPk->name . '">' . humanize($nonPk->name) . '</label>';

               // kondisi ada relasi
               // if (!empty($data['fk'])) {
               if (in_array($nonPk->name, $groupFk)) {
                  // pecah fk
                  foreach ($data['fk'] as $fks => $fk) {
                     if ($nonPk->name == $fk->column_name)
                        // type array (combobox)
                        $formTemplateEdit .= '
            <select name="' . $nonPk->name . '" class="custom-select">
               <?php foreach($data_' . $fk->foreign_table_name . ' as $' . plural($fk->foreign_table_name) . ' => $row): ?>
               <option value="<?= $row[\'' . $fk->foreign_column_name . '\'] ?>" <?= !empty($row[\'' . $nonPk->name . '\']) && $row[\'' . $nonPk->name . '\'] == $row[\'' . $fk->foreign_column_name . '\'] ? \'selected\' : \'\' ?>><?= $row[\'' . $fk->foreign_column_name . '\'] ?></option>
               <?php endforeach ?>
            </select>';
                     // .type array (combobox)

                  }
                  // .pecah fk
               }
               // .kondisi ada relasi

               // kondisi tidak ada relasi
               else {
                  // type text (textarea)
                  if ($nonPk->type == 'tinytext' || $nonPk->type == 'text' || $nonPk->type == 'mediumtext' || $nonPk->type == 'longtext') {
                     $formTemplateEdit .= '
            <textarea type="text" name="' . $nonPk->name . '" class="form-control" ><?= !empty($main[\'' . $nonPk->name . '\']) ? $main[\'' . $nonPk->name . '\'] : \'\' ?></textarea>';
                  }
                  // .type text (textarea)

                  // type enum (radio)
                  else if ($nonPk->type == 'enum' || $nonPk->type == 'set') {
                     $radio = 1;
                     foreach ($nonPk->option_value as $enum) {
                        $formTemplateEdit .= '
            <div class="form-check">
               <input class="form-check-input" type="radio" id="' . $nonPk->name . $radio . '" name="' . $nonPk->name . '" value="' . $enum . '" <?= !empty($main[\'' . $nonPk->name . '\']) && $main[\'' . $nonPk->name . '\'] == \'' . $enum . '\' ? \'checked\' : \'\' ?> />
               <label class="form-check-label" for="' . $nonPk->name . $radio . '">' . humanize($enum) . '</label>
            </div>';
                        $radio++;
                     }
                  }
                  // .type enum (radio)

                  // type time (time)
                  else if ($nonPk->type == 'time') {
                     $formTemplateEdit .= '
            <input type="text" name="' . $nonPk->name . '" value="<?= !empty($main[\'' . $nonPk->name . '\']) ? $main[\'' . $nonPk->name . '\'] : \'\' ?>" class="form-control time" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type time (time)

                  // type date (date)
                  else if ($nonPk->type == 'date') {
                     $formTemplateEdit .= '
            <input type="text" name="' . $nonPk->name . '" value="<?= !empty($main[\'' . $nonPk->name . '\']) ? $main[\'' . $nonPk->name . '\'] : \'\' ?>" class="form-control date" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type date (date)

                  // type datetime (datetime)
                  else if ($nonPk->type == 'datetime') {
                     $formTemplateEdit .= '
            <input type="text" name="' . $nonPk->name . '" value="<?= !empty($main[\'' . $nonPk->name . '\']) ? $main[\'' . $nonPk->name . '\'] : \'\' ?>" class="form-control datetime" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type datetime (datetime)

                  // type year (year)
                  else if ($nonPk->type == 'year') {
                     $formTemplateEdit .= '
            <input type="text" name="' . $nonPk->name . '" value="<?= !empty($main[\'' . $nonPk->name . '\']) ? $main[\'' . $nonPk->name . '\'] : \'\' ?>" class="form-control year" id="' . $nonPk->name . '" data-toggle="datetimepicker" data-target="#' . $nonPk->name . '" />';
                  }
                  // .type year (year)

                  // type blob (file)
                  else if ($nonPk->type == 'tinyblob' || $nonPk->type == 'blob' || $nonPk->type == 'mediumblob' || $nonPk->type == 'longblob') {
                     $formTemplateEdit .= '
            <input type="file"  name="' . $nonPk->name . '" class="form-control" id="file_' . $nonPk->name . '">';
                  }
                  // .type blob (file)

                  // type string (text)
                  else {
                     $formTemplateEdit .= '
            <input type="text" name="' . $nonPk->name . '" value="<?= !empty($main[\'' . $nonPk->name . '\']) ? $main[\'' . $nonPk->name . '\'] : \'\' ?>" class="form-control" />';
                  }
                  // .type string (text)
               }
               // .kondisi tidak ada relasi
               $formTemplateEdit .= '
            <div class="invalid-feedback ' . $nonPk->name . '"></div>
         </div>
      </div>
   </div>';
            }
         }

         $formTemplateEdit .= '
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>';

         // make form
         $formFileEdit = fopen(APPPATH . './../Modules/' . $data['controller'] . '/Views/edit.php', 'w')
            or $this->failServerError("Access to the path '" . APPPATH . "' is denied. Change your permision path to 777.");
         fwrite($formFileEdit, $formTemplateEdit);
         fclose($formFileEdit);
         // .make form
      }
   }
}

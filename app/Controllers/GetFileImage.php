<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class GetFileImage extends BaseController
{
	use ResponseTrait;
	public function index()
	{
		$path = $this->request->getVar('path');
		$imageName = $this->request->getVar('image');
		if (($image = file_get_contents(WRITEPATH . 'uploads/' . $path . '/' . $imageName)) === FALSE)
			show_404();

		// choose the right mime type
		$mimeType = 'image/jpg';

		$this->response
			->setStatusCode(200)
			->setContentType($mimeType)
			->setBody($image)
			->send();
	}
}

<?php

function new_uuid()
{
	// use uuid extension from PECL if available
	// if (function_exists("uuid_create")) {
	// 	return uuid_create();
	// }
	// fallback
	$uuid = md5(microtime() . getmypid());
	// this should be random enough for now
	// set variant and version fields for 'true' random uuid
	$uuid[12] = "4";
	$n = 8 + (ord($uuid[16]) & 3);
	$hex = "0123456789abcdef";
	$uuid[16] = $hex[$n];
	// return formated uuid
	return strtoupper(substr($uuid, 0, 8) . "-" . substr($uuid, 8, 4) . "-" . substr($uuid, 12, 4) . "-" . substr($uuid, 16, 4) . "-" . substr($uuid, 20));
}

function paginate($reload, $page, $tpages, $adjacents)
{
	$prevlabel 	= '<i class="fas fa-chevron-left"></i>';
	$nextlabel 	= '<i class="fas fa-chevron-right"></i>';
	$out 		= '<nav aria-label="..."><ul class="pagination pagination-large justify-content-end">';

	// previous label
	if ($page == 1) {
		$out .= '<li class="page-item disabled"><a class="page-link page-number">' . $prevlabel . '</a></li>';
	} else if ($page == 2) {
		$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="1" href="#/1">' . $prevlabel . '</a></li>';
	} else {
		$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="' . ($page - 1) . '" href="#/' . ($page - 1) . '">' . $prevlabel . '</a></li>';
	}

	// first label
	if ($page > ($adjacents + 1)) {
		$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="1" href="#/1">1</a></li>';
	}

	// interval
	if ($page > ($adjacents + 2)) {
		$out .= '<li class="page-item"><a class="page-link page-number">...</a></li>';
	}

	// pages
	$pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
	$pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
	for ($i = $pmin; $i <= $pmax; $i++) {
		if ($i == $page) {
			$out .= '<li class="page-item active"><a class="page-link page-number">' . $i . '</a></li>';
		} else if ($i == 1) {
			$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="1" href="#/1">' . $i . '</a></li>';
		} else {
			$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="' . $i . '" href="#/' . $i . '">' . $i . '</a></li>';
		}
	}

	// interval
	if ($page < ($tpages - $adjacents - 1)) {
		$out .= '<li class="page-item"><a class="page-link page-number">...</a></li>';
	}

	// last
	if ($page < ($tpages - $adjacents)) {
		$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="' . $tpages . '" href="#/' . ($tpages) . '">' . $tpages . '</a></li>';
	}

	// next
	if ($page < $tpages) {
		$out .= '<li class="page-item"><a class="page-link page-number" data-ci-pagination-page="' . ($page + 1) . '" href="#/' . ($tpages) . '">' . $nextlabel . '</a></li>';
	} else {
		$out .= '<li class="page-item disabled"><a class="page-link page-number">' . $nextlabel . '</a></li>';
	}

	$out .= '</ul></nav>';
	return $out;
}

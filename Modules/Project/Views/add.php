<form id="form-add" accept-charset="utf-8">
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="name_project">Name Project <span class="text-danger">*</span></label>
            <input type="text" name="name_project" class="form-control">
            <div class="invalid-feedback name_project"></div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="address">Address <span class="text-danger">*</span></label>
            <input type="text" name="address" class="form-control">
            <div class="invalid-feedback address"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="company">Company <span class="text-danger">*</span></label>
            <input type="text" name="company" class="form-control" />
            <div class="invalid-feedback company"></div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="hilight">Hilight <span class="text-danger">*</span></label>
            <select name="hilight" class="form-control custom-select">
               <option value="1">Ya</option>
               <option value="0">Tidak</option>
            </select>
            <div class="invalid-feedback hilight"></div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="position">Position <span class="text-danger">*</span></label>
            <input type="text" name="position" class="form-control" />
            <div class="invalid-feedback position"></div>
         </div>
      </div>
      <div class="col-md-12">
         <div class="form-group">
            <label for="AttachedDocument">Image</label>
            <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_1">
               <div class="dropzone-msg dz-message needsclick">
                  <h3 class="dropzone-msg-title">Drop files here or click to load</h3>
               </div>
            </div>
            <input type="hidden" name="AttachedDocument" class="form-control AttachedDocument" id="AttachedDocument" />
         </div>
      </div>
   </div>  
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
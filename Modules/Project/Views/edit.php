<form id="form-edit" accept-charset="utf-8">
    <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="name_project">Name Project</label>
            <textarea type="text" name="name_project" class="form-control" ><?= !empty($main['name_project']) ? $main['name_project'] : '' ?></textarea>
            <div class="invalid-feedback name_project"></div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="address">Address</label>
            <textarea type="text" name="address" class="form-control" ><?= !empty($main['address']) ? $main['address'] : '' ?></textarea>
            <div class="invalid-feedback address"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="company">Company</label>
            <input type="text" name="company" value="<?= !empty($main['company']) ? $main['company'] : '' ?>" class="form-control" />
            <div class="invalid-feedback company"></div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="hilight">Hilight</label>
            <select name="hilight" class="form-control custom-select">
               <option <?= !empty($main['hilight']) && $main['hilight'] == 1 ? 'selected' : '' ?> value="1">Ya</option>
               <option <?= !empty($main['hilight']) && $main['hilight'] == 0 ? 'selected' : '' ?> value="0">Tidak</option>
            </select>             
            <div class="invalid-feedback hilight"></div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            <label for="position">Position</label>
            <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
            <div class="invalid-feedback position"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="AttachedDocument">Attached Document</label>
            <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_1">
               <div class="dropzone-msg dz-message needsclick">
                  <h3 class="dropzone-msg-title">Drop files here or click to load</h3>
               </div>
            </div>
            <input type="hidden" value="<?= !empty($main['image']) ? $main['image'] : '' ?>" name="AttachedDocument" class="form-control AttachedDocument" id="AttachedDocument" />
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
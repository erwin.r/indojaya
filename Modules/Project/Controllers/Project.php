<?php
namespace Modules\Project\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Project\Models\ProjectModel;
class Project extends BaseController{
  use ResponseTrait;

  protected $ProjectModel;
  protected $edit;
  protected $delete;

  public function __construct() {
      $this->validation = \Config\Services::validation();
      $this->ProjectModel = new ProjectModel;
      $this->session = session();
      $this->edit = 0;
      $this->delete = 0;

  }

  function index(){
     $data = [
        'title' => 'Data Project',
        'host' => site_url('project/')
     ];
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     if($menu){
        $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
        if(!$this->ionAuth->hasAccess($menu->id,$groupId))
         echo view('errors\html\error_404'); 
        else{
         $data = [
           'title' => 'Data Project',
           'host' => site_url('project/'),
           'add' => $this->ionAuth->permissionAction($menu->id,'add',$groupId),
         ];
         echo view('Modules\Project\Views\list', $data);
        }
     } else echo view('errors\html\error_404'); 
  }

  public function data(){
      $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
      $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
      $this->edit = $this->ionAuth->permissionAction($menu->id,'edit',$groupId);
      $this->delete = $this->ionAuth->permissionAction($menu->id,'delete',$groupId);

      $where = "1=1";
      $request = esc($this->request->getPost());
      $name_project = $request['name_project'];
      $address = $request['address'];
      $company = $request['company'];
      $image = $request['image'];
      $hilight = $request['hilight'];
      $position = $request['position'];
      
      $cols = array();
      if (!empty($name_project)) { $cols['name_project'] = $name_project; }
      if (!empty($address)) { $cols['address'] = $address; }
      if (!empty($company)) { $cols['company'] = $company; }
      if (!empty($image)) { $cols['image'] = $image; }
      if (!empty($hilight)) { $cols['hilight'] = $hilight; }
      if (!empty($position)) { $cols['position'] = $position; }
      
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->ProjectModel->filter($cols, $search, $limit, $start, $orderFields, $orderDir,$where)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '';
            
            $btn_actions .= '
                           <div class="dropdown dropdown-inline mr-4 dropright">
                              <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="ki ki-bold-more-hor"></i>
                              </button>
                              <div class="dropdown-menu">
                                 <ul class="navi navi-hover">';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-edit" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-pencil-alt text-warning"></i>
                                          </span>
                                          <span class="navi-text">Edit</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-delete" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-trash text-danger"></i>
                                          </span>
                                          <span class="navi-text">Delete</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                 </ul>
                              </div>
                           </div>';
            
            $row[] = $btn_actions;
            $row[] = $no;
            $row[] = $r->name_project;
            $row[] = $r->address;
            $row[] = $r->company;
            $row[] = $r->image;
            $row[] = $r->hilight;
            $row[] = $r->position;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->ProjectModel->countTotal($where),
         'recordsFiltered' => $this->ProjectModel->countFilter($cols, $search, $where),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
   ];
   return view('Modules\Project\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
               'name_project' => $this->request->getPost('name_project'),
               'address' => $this->request->getPost('address'),
               'company' => $this->request->getPost('company'),
               'image' => str_replace(str_split('\\/:*?"<>|- '), '', $this->request->getPost('AttachedDocument')),
               'hilight' => $this->request->getPost('hilight'),
               'position' => $this->request->getPost('position'),
              'user_created' => $this->session->get('user_id'),
         ];

         if ($this->ProjectModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->ProjectModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
      ];
      $data['main'] = $this->ProjectModel->find($id);
      return view('Modules\Project\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
               'name_project' => $this->request->getPost('name_project'),
               'address' => $this->request->getPost('address'),
               'company' => $this->request->getPost('company'),
               'image' => str_replace(str_split('\\/:*?"<>|- '), '', $this->request->getPost('AttachedDocument')),
               'hilight' => $this->request->getPost('hilight'),
               'position' => $this->request->getPost('position'),
              'user_update' => $this->session->get('user_id'),
         ];
         if ($this->ProjectModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->ProjectModel->errors());
      }
    }

    public function import()
    {
      $file = $this->request->getFile('file');
      $fileName = $file->getRandomName();      
      if (($file->move(WRITEPATH . "/uploads/project", $fileName))) {      
         echo json_encode($fileName);
      }
    }

    public function delete($id)
    {
        if ($found = $this->ProjectModel->where('id', $id)->delete()) {
            return $this->respondDeleted($found);
        }
        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'name_project' => [
            'label' => 'Name Project',
            'rules' => 'required|string'
         ],
         'address' => [
            'label' => 'Address',
            'rules' => 'required|string'
         ],
         'company' => [
            'label' => 'Company',
            'rules' => 'required|string|max_length[50]'
         ],
         'AttachedDocument' => [
            'label' => 'AttachedDocument',
            'rules' => 'required|string'
         ],
         'hilight' => [
            'label' => 'Hilight',
            'rules' => 'required|numeric|max_length[1]'
         ],
         'position' => [
            'label' => 'Position',
            'rules' => 'required|numeric|max_length[2]'
         ],
      ]);
   }

}
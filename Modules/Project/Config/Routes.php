<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('project', ['namespace' => 'Modules\Project\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'Project::index');
          $routes->get('edit', 'Project::load_form_edit');
          $routes->get('add', 'Project::load_form_add');
          $routes->post('updated', 'Project::updated');
          $routes->post('addnew', 'Project::new');
          $routes->post('data', 'Project::data');
          $routes->post('import', 'Project::import');
          $routes->resource('resources', [
              'controller' => 'Project',
          ]);
      });
      
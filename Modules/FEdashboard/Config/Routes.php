<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('', ['namespace' => 'Modules\FEdashboard\Controllers'], function($routes)
      {
          $routes->get('/', 'FEdashboard::index');         
          $routes->resource('resources', [
              'controller' => 'FEdashboard',
          ]);
      });
      
<?php
namespace Modules\FEdashboard\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\FEdashboard\Models\FEdashboardModel;
class FEdashboard extends BaseController{
   use ResponseTrait;   

   public function __construct() {
   $this->validation = \Config\Services::validation();
   $this->FEdashboardModel = new FEdashboardModel;
   $this->db = \Config\Database::connect();

   }

   function index(){
      $data = [
        'title'   => 'Dashboard',
        'host'    => site_url('/'),
        'category'=> $this->FEdashboardModel->getCategoryProduct('hilight=1')->get()->getResult(),
        'top_produk'=> $this->FEdashboardModel->getTopProduct('1=1')->get()->getResult(),
        'installation'=> $this->FEdashboardModel->getInstallation('1=1')->get()->getResult()
      ];
      echo view('Modules\FEdashboard\Views\list', $data);
   }

}
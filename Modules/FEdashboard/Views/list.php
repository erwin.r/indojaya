<?= $this->include('layout/frontend/header') ?>     
    <div class="electro-product-wrapper wrapper-padding pb-45">
        <div class="container-fluid">
            <div class="section-title-4 text-center mb-40">
                <h2>Top Produk</h2>
            </div>
            <div class="top-product-style">
                <div class="product-tab-list3 text-center mb-80 nav product-menu-mrg" role="tablist">
                    <?php $n=1; foreach ($category as $r) {?>
                        <a class="<?php if($n==1) echo "active"; ?>" href="#<?php echo $r->link; ?>" data-bs-toggle="tab" role="tab">
                            <h4><?php echo $r->category; ?></h4>
                        </a>
                    <?php $n++; } ?>                    
                </div>
                <div class="tab-content">
                    <?php $n=1; foreach ($category as $r) { ?>
                    <div class="tab-pane <?php if($n==1) echo "active"; ?> show fade" id="<?php echo $r->link; ?>" role="tabpanel">
                        <div class="custom-row-2">
                            <?php 
                                $this->ionAuth = new \IonAuth\Libraries\IonAuth();                                    
                                $setting = $this->ionAuth->getSetting();
                                $produk=$this->ionAuth->getProduct('category_product.link="'.$r->link.'"')->get()->getResult(); 
                                foreach ($produk as $p){ 
                            ?>
                            <div class="custom-col-style-2 custom-col-4">
                                <div class="product-wrapper product-border mb-24">
                                    <div class="product-img-3">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=product/".$p->link."&image=" . $p->image) : "" ?>" class="lazyload" alt="Image">                                        
                                    </div>
                                    <div class="product-img-3">                                        
                                        <div class="product-action-right">
                                            <a class="animate-right" href="#" data-bs-target="#Modal<?= $p->id; ?>" data-bs-toggle="modal" title="Quick View">
                                                <i class="pe-7s-look"></i>
                                            </a>
                                            <a class="animate-top" title="Pesan Sekarang" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $p->name_product; ?>"">
                                                <i class="pe-7s-cart"></i>
                                            </a>                                            
                                        </div>
                                    </div>
                                    <div class="product-content-4 text-center">                                        
                                        <h4><?php echo $p->name_product; ?></h4>                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="modal fade" id="Modal<?= $p->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span class="pe-7s-close" aria-hidden="true"></span>
                                </button>
                                <div class="modal-dialog modal-quickview-width" role="document">
                                    <div class="modal-content">                                        
                                        <img data-src="<?= base_url() ?>/theme/frontend/img/product/<?= $p->link; ?>/<?= $p->image; ?>" class="lazyload img img-responsive" alt="">                                                                                
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php $n++; } ?>                    
                </div>
            </div>
        </div>
    </div>
    <?php 
        $this->ionAuth = new \IonAuth\Libraries\IonAuth();
        $setting = $this->ionAuth->getSetting();
    ?>
    <?php if($setting->field3 != ''){ ?>
    <div class="electro-product-wrapper wrapper-padding pb-45">
        <div class="container-fluid">
            <div class="section-title-4 text-center mb-40">
                <h2>Proses Pemasangan Plafon</h2>
            </div>
            <div class="custom-container">
                <div class="coustom-row-3">                
                    <iframe width="100%" height="400" src="<?= $setting->field3; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
                <div class="mt-2"></div>
                <div class="coustom-row-3">
                    <?php foreach ($installation as $p){ ?>
                    <div class="custom-col-style-3 custom-col-3">
                        <div class="product-wrapper mb-6">
                            <div class="product-img-4">                                            
                                <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=installation&image=" . $p->image) : "" ?>" class="lazyload" alt="Image">                                        
                                <div class="product-action-right">
                                    <a class="animate-right" href="#" data-bs-target="#ModalTP<?= $p->id; ?>" data-bs-toggle="modal" title="Quick View">
                                        <i class="pe-7s-look"></i>
                                    </a>                                     
                                </div>
                            </div>
                            <div class="product-content-6">
                                <h4><a href="product-details.html">&nbsp;</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="ModalInstallation<?= $p->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span class="pe-7s-close" aria-hidden="true"></span>
                        </button>
                        <div class="modal-dialog modal-quickview-width" role="document">
                            <div class="modal-content">                                        
                                <img data-src="<?= base_url() ?>/theme/frontend/img/installation/<?= $p->image; ?>" class="lazyload img img-responsive" alt="">                                                                                
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>    
        </div>
    </div>
    <?php };?>
    <!-- <div class="best-selling-area pb-95">
        <div class="section-title-4 text-center mb-60">
            <h2>Produk Unggulan</h2>
        </div>
        <div class="best-selling-product">
            <div class="row g-0">
                <div class="col-lg-12">
                    <div class="best-selling">
                        <div class="custom-container">
                            <div class="coustom-row-3">
                                <?php foreach ($top_produk as $p){ ?>
                                <div class="custom-col-style-3 custom-col-3">
                                    <div class="product-wrapper mb-6">
                                        <div class="product-img-4">                                            
                                            <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=product/".$p->link."&image=" . $p->image) : "" ?>" class="lazyload" alt="Image">                                        
                                            <div class="product-action-right">
                                                <a class="animate-right" href="#" data-bs-target="#ModalTP<?= $p->id; ?>" data-bs-toggle="modal" title="Quick View">
                                                    <i class="pe-7s-look"></i>
                                                </a>
                                                <a class="animate-top" title="Pesan Sekarang" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $p->name_product; ?>"">
                                                    <i class="pe-7s-cart"></i>
                                                </a>   
                                            </div>
                                        </div>
                                        <div class="product-content-6">
                                            <h4><a href="product-details.html">&nbsp;</a></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="ModalTP<?= $p->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                        <span class="pe-7s-close" aria-hidden="true"></span>
                                    </button>
                                    <div class="modal-dialog modal-quickview-width" role="document">
                                        <div class="modal-content">                                        
                                            <img data-src="<?= base_url() ?>/theme/frontend/img/product/<?= $p->link; ?>/<?= $p->image; ?>" class="lazyload img img-responsive" alt="">                                                                                
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="brand-logo-area-2 wrapper-padding ptb-80">
        <div class="container-fluid">
            <div class="brand-logo-active2 owl-carousel">
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/7.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/8.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/9.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/10.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/11.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/12.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/13.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/7.png" alt="">
                </div>
                <div class="single-brand">
                    <img src="<?= base_url() ?>/theme/frontend/img/brand-logo/8.png" alt="">
                </div>
            </div>
        </div>
    </div> -->
    <?= $this->include('layout/frontend/footer') ?>
    <!-- all js here -->
    <script src="<?= base_url() ?>/theme/frontend/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/popper.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/isotope.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/waypoints.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/ajax-mail.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/plugins.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/main.js"></script>
</body>

</html>
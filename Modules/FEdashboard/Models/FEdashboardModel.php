<?php
namespace Modules\FEdashboard\Models;
use CodeIgniter\Model;
use App\Controllers\BaseController;

class FEdashboardModel extends Model{
   protected $category_product = 'category_product';
   protected $product = 'product';
   protected $top_product = 'top_product';
   protected $installation = 'installation';   
   
   public function getCategoryProduct($where)
   {
      $builder=$this->db->table($this->category_product);
      $builder->select('category,link')
              ->orderBy('position','ASC')->where('deleted_at Is Null AND '.$where);
      return $builder;
   }

   public function getProduct($where)
   {
      $builder = $this->db->table($this->product);
      $builder ->select('product.name_product,product.image,category_product.link')
               ->join('category_product','product.category=category_product.id')
               ->orderBy('product.position','ASC')->where('product.deleted_at Is Null AND '.$where);
      return $builder;
   }

   public function getTopProduct($where)
   {
      $builder = $this->db->table($this->top_product);
      $builder ->select('top_product.id,top_product.name_product,top_product.image,category_product.link')
               ->join('category_product','top_product.category=category_product.id')
               ->orderBy('top_product.position','ASC')->where('top_product.deleted_at Is Null AND '.$where);
      return $builder;
   }

   public function getInstallation($where)
   {
      $builder=$this->db->table($this->installation);
      $builder->select('id,image,description')
              ->orderBy('position','ASC')->where('deleted_at Is Null AND '.$where);
      return $builder;
   }
}
<?php
namespace Modules\FEproduct\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\FEproduct\Models\FEproductModel;
use Modules\FEproject\Models\FEprojectModel;
class FEproduct extends BaseController{
   use ResponseTrait;   

   public function __construct() {
      $this->validation = \Config\Services::validation();
      $this->FEproductModel = new FEproductModel;
      $this->FEprojectModel = new FEprojectModel;
      $this->db = \Config\Database::connect();
   }

   function index($produk){
      $data = [
        'title'   => 'Produk',
        'host'    => site_url('/'),
        'category'=> $this->FEproductModel->getCategoryProduct('link="'.$produk.'"')->get()->getRow(),
        'produk'  => $this->FEproductModel->getProduct('link="'.$produk.'"')->get()->getResult(),
        'service' => $this->FEprojectModel->getJasa('category_product.link="'.$produk.'"')->get()->getResult()
      ];
      echo view('Modules\FEproduct\Views\list', $data);
   }

   function cari_produk(){
      $param=$this->request->getGet('query');
      $category=$this->request->getGet('kategori');    
      if ($category=='semua') {
         $category_show='Semua Kategori'; 
         $queryProduk=$this->FEproductModel->getProduct('name_product LIKE "%'.$param.'%"')->get()->getResult();
         $queryService=$this->FEprojectModel->getJasa('service.service_name LIKE "%'.$param.'%"')->get()->getResult();
      } else {
         $row_category = $this->FEproductModel->getCategoryProduct('link="'.$category.'"')->get()->getRow();
         $category_show = $row_category->category;
         $queryProduk = $this->FEproductModel->getProduct('category_product.link="'.$category.'" AND product.name_product LIKE "%'.$param.'%"')->get()->getResult();
         $queryService=$this->FEprojectModel->getJasa('category_product.link="'.$category.'" AND service.service_name LIKE "%'.$param.'%"')->get()->getResult();
      }
      $data = [
        'title'   => 'Cari Produk',        
        'host'    => site_url('/'),
        'category' => $category_show,
        'produk'=> $queryProduk,
        'service' => $queryService,
      ];
      echo view('Modules\FEproduct\Views\list', $data);
   }

}
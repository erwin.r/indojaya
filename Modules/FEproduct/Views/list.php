<?= $this->include('layout/frontend/header') ?>        
    <div class="electro-product-wrapper wrapper-padding pb-45">
        <div class="container-fluid">
            <div class="section-title-4 text-center mb-40">
                <h2><?php                      
                    $this->ionAuth = new \IonAuth\Libraries\IonAuth();                                    
                    $setting = $this->ionAuth->getSetting();                  
                    echo isset($category->category)?$category->category:$category; ?></h2>
            </div>
            <div class="top-product-style">                
                <div class="tab-content">                                        
                        <div class="custom-row-2">
                            <?php foreach ($produk as $p){ ?>
                                <div class="custom-col-style-2 custom-col-4">
                                <div class="product-wrapper product-border mb-24">
                                    <div class="product-img-3">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=product/".$p->link."&image=" . $p->image) : "" ?>" class="lazyload" alt="Image">                                        
                                    </div>
                                    <div class="product-img-3">                                        
                                        <div class="product-action-right">
                                            <a class="animate-right" href="#" data-bs-target="#Modal<?= $p->id; ?>" data-bs-toggle="modal" title="Quick View">
                                                <i class="pe-7s-look"></i>
                                            </a>
                                            <a class="animate-top" title="Pesan Sekarang" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $p->name_product; ?>"">
                                                <i class="pe-7s-cart"></i>
                                            </a>                                            
                                        </div>
                                    </div>
                                    <div class="product-content-4 text-center">                                        
                                        <h4><?php echo $p->name_product; ?></h4>                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="modal fade" id="Modal<?= $p->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span class="pe-7s-close" aria-hidden="true"></span>
                                </button>
                                <div class="modal-dialog modal-quickview-width" role="document">
                                    <div class="modal-content">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=product/".$p->link."&image=" . $p->image) : "" ?>" class="lazyload img img-responsive" alt="">                                                                                
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>                    
                </div>            
                <div class="tab-content">
                    <div class="section-title-4 text-center mb-40">
                        <h2>Jasa</h2>
                    </div>
                    <div class="tab-pane active show fade" role="tabpanel">
                        <div class="custom-row-2">
                            <?php                                 
                                foreach ($service as $p){ 
                            ?>
                            <div class="custom-col-style-2 custom-col-4">
                                <div class="product-wrapper product-border mb-24">
                                    <div class="product-img-3">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=jasa&image=" . $p->image) : "" ?>" class="lazyload" alt="Image">                                        
                                    </div>
                                    <div class="product-img-3">                                        
                                        <div class="product-action-right">
                                            <a class="animate-right" href="#" data-bs-target="#Modal<?= $p->id; ?>" data-bs-toggle="modal" title="Quick View">
                                                <i class="pe-7s-look"></i>
                                            </a>
                                            <a class="animate-top" title="Pesan Sekarang" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $p->service_name; ?>"">
                                                <i class="pe-7s-cart"></i>
                                            </a>                                            
                                        </div>
                                    </div>
                                    <div class="product-content-4 text-center">                                        
                                        <h4><?php echo $p->service_name; ?></h4>                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="modal fade" id="Modal<?= $p->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span class="pe-7s-close" aria-hidden="true"></span>
                                </button>
                                <div class="modal-dialog modal-quickview-width" role="document">
                                    <div class="modal-content">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=jasa&image=" . $p->image) : "" ?>" class="lazyload img img-responsive" alt="">                                                                                
                                    </div>
                                </div>
                            </div>
                            <?php } ?>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->include('layout/frontend/footer') ?>
    <!-- all js here -->
    <script src="<?= base_url() ?>/theme/frontend/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/popper.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/isotope.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/waypoints.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/ajax-mail.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/plugins.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/main.js"></script>
</body>

</html>
<?php
namespace Modules\FEproduct\Models;
use CodeIgniter\Model;
use App\Controllers\BaseController;

class FEproductModel extends Model{
   protected $category_product = 'category_product';
   protected $product = 'product';
   
   public function getCategoryProduct($where)
   {
      $builder=$this->db->table($this->category_product);
      $builder->select('category,link')
              ->orderBy('position','ASC')->where('deleted_at Is Null AND '.$where);
      return $builder;
   }

   public function getProduct($where)
   {
      $builder = $this->db->table($this->product);
      $builder ->select('product.id,product.name_product,category_product.link,product.image')
               ->join('category_product','product.category=category_product.id')
               ->orderBy('product.position','ASC')->where('product.deleted_at Is Null AND '.$where);
      return $builder;
   }
}
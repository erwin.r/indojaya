<form id="form-add" accept-charset="utf-8">
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="contac">Contac <span class="text-danger">*</span></label>
            <input type="text" name="contac" class="form-control" />
            <div class="invalid-feedback contac"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="field1">Field1 <span class="text-danger">*</span></label>
            <input type="text" name="field1" class="form-control" />
            <div class="invalid-feedback field1"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="field2">Field2 <span class="text-danger">*</span></label>
            <input type="text" name="field2" class="form-control" />
            <div class="invalid-feedback field2"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="field3">Field3 <span class="text-danger">*</span></label>
            <input type="text" name="field3" class="form-control" />
            <div class="invalid-feedback field3"></div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
<form id="form-edit" accept-charset="utf-8">
    <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="contac">Contac</label>
            <input type="text" name="contac" value="<?= !empty($main['contac']) ? $main['contac'] : '' ?>" class="form-control" />
            <div class="invalid-feedback contac"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="field1">Field1</label>
            <input type="text" name="field1" value="<?= !empty($main['field1']) ? $main['field1'] : '' ?>" class="form-control" />
            <div class="invalid-feedback field1"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="field2">Field2</label>
            <input type="text" name="field2" value="<?= !empty($main['field2']) ? $main['field2'] : '' ?>" class="form-control" />
            <div class="invalid-feedback field2"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="field3">Field3</label>
            <input type="text" name="field3" value="<?= !empty($main['field3']) ? $main['field3'] : '' ?>" class="form-control" />
            <div class="invalid-feedback field3"></div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
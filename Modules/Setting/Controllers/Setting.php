<?php
namespace Modules\Setting\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Setting\Models\SettingModel;
class Setting extends BaseController{
  use ResponseTrait;

  protected $SettingModel;
  protected $edit;
  protected $delete;

  public function __construct() {
      $this->validation = \Config\Services::validation();
      $this->SettingModel = new SettingModel;
      $this->session = session();
      $this->edit = 0;
      $this->delete = 0;

  }

  function index(){
     $data = [
        'title' => 'Data Setting',
        'host' => site_url('setting/')
     ];
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     if($menu){
        $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
        if(!$this->ionAuth->hasAccess($menu->id,$groupId))
         echo view('errors\html\error_404'); 
        else{
         $data = [
           'title' => 'Data Setting',
           'host' => site_url('setting/'),
           'add' => $this->ionAuth->permissionAction($menu->id,'add',$groupId),
         ];
         echo view('Modules\Setting\Views\list', $data);
        }
     } else echo view('errors\html\error_404'); 
  }

  public function data(){
      $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
      $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
      $this->edit = $this->ionAuth->permissionAction($menu->id,'edit',$groupId);
      $this->delete = $this->ionAuth->permissionAction($menu->id,'delete',$groupId);

      $where = "1=1";
      $request = esc($this->request->getPost());
      $contac = $request['contac'];
      $field1 = $request['field1'];
      $field2 = $request['field2'];
      $field3 = $request['field3'];
      
      $cols = array();
      if (!empty($contac)) { $cols['contac'] = $contac; }
      if (!empty($field1)) { $cols['field1'] = $field1; }
      if (!empty($field2)) { $cols['field2'] = $field2; }
      if (!empty($field3)) { $cols['field3'] = $field3; }
      
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->SettingModel->filter($cols, $search, $limit, $start, $orderFields, $orderDir,$where)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '';
            
            $btn_actions .= '
                           <div class="dropdown dropdown-inline mr-4 dropright">
                              <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="ki ki-bold-more-hor"></i>
                              </button>
                              <div class="dropdown-menu">
                                 <ul class="navi navi-hover">';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-edit" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-pencil-alt text-warning"></i>
                                          </span>
                                          <span class="navi-text">Edit</span>
                                       </a>
                                    </li>';            
            $btn_actions .= '
                                 </ul>
                              </div>
                           </div>';
            
            $row[] = $btn_actions;
            $row[] = $no;
            $row[] = $r->contac;
            $row[] = $r->field1;
            $row[] = $r->field2;
            $row[] = $r->field3;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->SettingModel->countTotal($where),
         'recordsFiltered' => $this->SettingModel->countFilter($cols, $search, $where),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
   ];
   return view('Modules\Setting\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
               'contac' => $this->request->getPost('contac'),
               'field1' => $this->request->getPost('field1'),
               'field2' => $this->request->getPost('field2'),
               'field3' => $this->request->getPost('field3'),
              'user_created' => $this->session->get('user_id'),
         ];

         if ($this->SettingModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->SettingModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
      ];
      $data['main'] = $this->SettingModel->find($id);
      return view('Modules\Setting\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
   
               'contac' => $this->request->getPost('contac'),
               'field1' => $this->request->getPost('field1'),
               'field2' => $this->request->getPost('field2'),
               'field3' => $this->request->getPost('field3'),
              'user_update' => $this->session->get('user_id'),
         ];
         if ($this->SettingModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->SettingModel->errors());
      }
    }

    public function delete($id)
    {
        if ($found = $this->SettingModel->where('id', $id)->delete()) {
            return $this->respondDeleted($found);
        }
        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'contac' => [
            'label' => 'Contac',
            'rules' => 'required|string|max_length[30]'
         ],
      ]);
   }

}
<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('setting', ['namespace' => 'Modules\Setting\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'Setting::index');
          $routes->get('edit', 'Setting::load_form_edit');
          $routes->get('add', 'Setting::load_form_add');
          $routes->post('updated', 'Setting::updated');
          $routes->post('addnew', 'Setting::new');
          $routes->post('data', 'Setting::data');
          $routes->resource('resources', [
              'controller' => 'Setting',
          ]);
      });
      
<form id="form-edit" accept-charset="utf-8">
   <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">   
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="link">Category</label>
            <select name="link" class="form-control custom-select linkcategory">
               <option <?= !empty($main['link']) && $main['link'] == '#' ? 'selected' : '' ?> value="#">-</option>
               <?php foreach ($category as $r) {?>
                  <option <?= !empty($main['link']) && $main['link'] == $r['link'] ? 'selected' : '' ?> value="<?= $r['link'] ?>"><?= $r['category'] ?></option>
               <?php } ?>
            </select>                
            <div class="invalid-feedback link"></div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="message">Message</label>
            <textarea type="text" name="message" class="form-control" ><?= !empty($main['message']) ? $main['message'] : '' ?></textarea>
            <div class="invalid-feedback message"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="hilight">Hilight</label>
            <select name="hilight" class="form-control custom-select">
               <option <?= !empty($main['hilight']) && $main['hilight'] == 1 ? 'selected' : '' ?> value="1">Ya</option>
               <option <?= !empty($main['hilight']) && $main['hilight'] == 0 ? 'selected' : '' ?> value="0">Tidak</option>
            </select> 
            <div class="invalid-feedback hilight"></div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="position">Position</label>
            <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
            <div class="invalid-feedback position"></div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="type">Type</label>
            <div class="form-check">
               <input class="form-check-input" type="radio" id="type1" name="type" value="Desktop" <?= !empty($main['type']) && $main['type'] == 'Desktop' ? 'checked' : '' ?> />
               <label class="form-check-label" for="type1">Desktop</label>
            </div>
            <div class="form-check">
               <input class="form-check-input" type="radio" id="type2" name="type" value="Mobile" <?= !empty($main['type']) && $main['type'] == 'Mobile' ? 'checked' : '' ?> />
               <label class="form-check-label" for="type2">Mobile</label>
            </div>
            <div class="invalid-feedback type"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="AttachedDocument">Attached Document</label>
            <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_1">
               <div class="dropzone-msg dz-message needsclick">
                  <h3 class="dropzone-msg-title">Drop files here or click to load</h3>
               </div>
            </div>
            <input type="hidden" value="<?= !empty($main['image']) ? $main['image'] : '' ?>" name="AttachedDocument" class="form-control AttachedDocument" id="AttachedDocument" />
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('slider', ['namespace' => 'Modules\Slider\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'Slider::index');
          $routes->get('edit', 'Slider::load_form_edit');
          $routes->get('add', 'Slider::load_form_add');
          $routes->post('updated', 'Slider::updated');
          $routes->post('addnew', 'Slider::new');
          $routes->post('data', 'Slider::data');
          $routes->post('import', 'Slider::import');
          $routes->resource('resources', [
              'controller' => 'Slider',
          ]);
      });
      
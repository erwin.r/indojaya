<form id="form-add" accept-charset="utf-8">
   <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" value="<?= !empty($main['name']) ? $main['name'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <label for="description">Description</label>
      <input type="text" name="description" value="<?= !empty($main['description']) ? $main['description'] : '' ?>" class="form-control" />
   </div>
   

   <div class="form-group">
      <button type="submit" class="submit btn btn-primary font-weight-bold">Save</button>
      <button type="button" class="closed btn btn-secondary font-weight-bold" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
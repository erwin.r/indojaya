<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('groups', ['namespace' => 'Modules\Groups\Controllers'], function($routes)
      {
          $routes->get('/', 'Groups::index');
          $routes->get('edit', 'Groups::load_form_edit');
          $routes->get('add', 'Groups::load_form_add');
          $routes->post('updated', 'Groups::updated');
          $routes->post('addnew', 'Groups::new');
          $routes->post('data', 'Groups::data');
          $routes->resource('resources', [
              'controller' => 'Groups',
          ]);
      });
      
<?php
namespace Modules\FEabout\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\FEabout\Models\FEaboutModel;
class FEabout extends BaseController{
   use ResponseTrait;   

   public function __construct() {
   $this->validation = \Config\Services::validation();
   $this->FEaboutModel = new FEaboutModel;
   $this->db = \Config\Database::connect();

   }

   function index(){
      $data = [
        'title' => 'About',
        'host' => site_url('/about')
      ];
      echo view('Modules\FEabout\Views\list', $data);
   }

}
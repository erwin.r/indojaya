<?= $this->include('layout/frontend/header') ?>                
    <div class="choose-area pb-100">
        <div class="container">
            <div class="about-section">
                <h3>CV. Indojaya Cemelang</h3>
                <p style="text-align: justify;">Didirikan di Makassar pada tanggal 21 Februari  2011  dengan akta pendirian perusahaan No : AHU-0077307-AH.01.14 Tahun 2021 yang dibuat didepan Notaris Cahyo Rahadian Muzhar, SH., LLM di Makassar pada tanggal 01 Desember 2021. Perusahaan ini telah mengerjakan beberapa jenis pekerjaan, baik itu berupa perumahan maupun beberapa hotel dan perkantoran</p>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-12">
                    <div class="all-causes">
                        <div class="row">
                            <div class="col-md-6 causes-res">
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Atap Bajaringan</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Atap Bajaringan</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Plafon Gypsum</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Plafon Gypsum</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">List Gypsum</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan List Gypsum</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Partisi Gypsum</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Partisi Gypsum</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Moulding</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Moulding</p>
                                </div>
                            </div>
                            <div class="col-md-6">                                
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Plafon PVC</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Plafon PVC</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Partisi PVC</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Partisi PVC</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Cutting Board PVC</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Cutting Board PVC</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Lis PVC</h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Lis PVC</p>
                                </div>
                                <div class="choose-wrapper">
                                    <h4 class="uppercase">Ornamen PVC  </h4>
                                    <p>Kami menyediakan produk dan jasa pemasangan Ornamen PVC  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12">
                    <div class="choose-banner-wrapper f-right">
                        <img class="lazyload" data-src="<?= base_url() ?>/theme/frontend/img/logo/logo.jpg" alt="">                                                
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="goal-area pb-65">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="goal-wrapper mb-30">
                        <h3>VISI KAMI</h3>
                        <p>Menjadikan CV. Indojaya Cemelang sebagai perusahaan konstruksi yang berdaya saing tinggi, tumbuh berkelanjutan dan mampu memberikan konribusi besar bagi pembangunan daerah, serta mampu bersaing secara nasional melalui kompetisi tinggi yang berkesinambungan</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="goal-wrapper mb-30">
                        <h3>MISI KAMI</h3>                        
                        <p>1. Menjadi mitra pelanggan yang paling dapat diandalkan dalam bidang pembangunan dan konstruksi baik barang dan jasa</p>
                        <p>2. Meningkatkan kualitas kerja karyawan dalam lingkungan kerja yang semakin kondusif, sekaligus mambantu mereka dalam mencapai kesejahteraan          
                        </p>
                        <p>3. Mengutamakan kualitas, kecepatan dan kejujuran dalam bekerja          
                        </p>                        
                    </div>
                </div>                
            </div>
        </div>
    </div>    
    <!-- testimonials area end -->
    <?= $this->include('layout/frontend/footer') ?>
    <!-- all js here -->
    <script src="<?= base_url() ?>/theme/frontend/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/popper.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/isotope.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/waypoints.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/ajax-mail.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/plugins.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/main.js"></script>
</body>

</html>
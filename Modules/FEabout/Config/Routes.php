<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('tentang', ['namespace' => 'Modules\FEabout\Controllers'], function($routes)
      {
          $routes->get('/', 'FEabout::index');          
          $routes->resource('resources', [
              'controller' => 'FEabout',
          ]);
      });
      
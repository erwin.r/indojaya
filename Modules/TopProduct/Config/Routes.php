<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('topproduct', ['namespace' => 'Modules\TopProduct\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'TopProduct::index');
          $routes->get('edit', 'TopProduct::load_form_edit');
          $routes->get('add', 'TopProduct::load_form_add');
          $routes->post('updated', 'TopProduct::updated');
          $routes->post('addnew', 'TopProduct::new');
          $routes->post('data', 'TopProduct::data');
          $routes->post('import', 'TopProduct::import');
          $routes->resource('resources', [
              'controller' => 'TopProduct',
          ]);
      });
      
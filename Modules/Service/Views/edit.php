<form id="form-edit" accept-charset="utf-8">
    <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="service_name">Service Name</label>
            <textarea type="text" name="service_name" class="form-control" ><?= !empty($main['service_name']) ? $main['service_name'] : '' ?></textarea>
            <div class="invalid-feedback service_name"></div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="category">Category</label>
            <select name="category" class="form-control custom-select linkcategory">
               <?php foreach ($category as $r) {?>
                  <option <?= !empty($main['category']) && $main['category'] == $r['id'] ? 'selected' : '' ?> value="<?= $r['id'] ?>"><?= $r['category'] ?></option>
               <?php } ?>
            </select>                 
            <div class="invalid-feedback category"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="hilight">Hilight</label>
            <select name="hilight" class="form-control custom-select">
               <option <?= !empty($main['hilight']) && $main['hilight'] == 1 ? 'selected' : '' ?> value="1">Ya</option>
               <option <?= !empty($main['hilight']) && $main['hilight'] == 0 ? 'selected' : '' ?> value="0">Tidak</option>
            </select>             
            <div class="invalid-feedback hilight"></div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="position">Position</label>
            <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
            <div class="invalid-feedback position"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="AttachedDocument">Attached Document</label>
            <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_1">
               <div class="dropzone-msg dz-message needsclick">
                  <h3 class="dropzone-msg-title">Drop files here or click to load</h3>
               </div>
            </div>
            <input type="hidden" value="<?= !empty($main['image']) ? $main['image'] : '' ?>" name="AttachedDocument" class="form-control AttachedDocument" id="AttachedDocument" />
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
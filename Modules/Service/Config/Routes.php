<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('service', ['namespace' => 'Modules\Service\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'Service::index');
          $routes->get('edit', 'Service::load_form_edit');
          $routes->get('add', 'Service::load_form_add');
          $routes->post('updated', 'Service::updated');
          $routes->post('addnew', 'Service::new');
          $routes->post('data', 'Service::data');
          $routes->post('import', 'Service::import');
          $routes->resource('resources', [
              'controller' => 'Service',
          ]);
      });
      
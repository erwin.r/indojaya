<?php
namespace Modules\Product\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Product\Models\ProductModel;
use Modules\CategoryProduct\Models\CategoryProductModel;
class Product extends BaseController{
  use ResponseTrait;

  protected $ProductModel;
  protected $edit;
  protected $delete;

  public function __construct() {
      $this->validation = \Config\Services::validation();
      $this->ProductModel = new ProductModel;
      $this->CategoryProductModel = new CategoryProductModel;
      $this->session = session();
      $this->edit = 0;
      $this->delete = 0;

  }

  function index(){
     $data = [
        'title' => 'Data Product',
        'host' => site_url('product/')
     ];
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     if($menu){
        $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
        if(!$this->ionAuth->hasAccess($menu->id,$groupId))
         echo view('errors\html\error_404'); 
        else{
         $data = [
           'title' => 'Data Product',
           'host' => site_url('product/'),
           'add' => $this->ionAuth->permissionAction($menu->id,'add',$groupId),
           'category' => $this->CategoryProductModel->findAll(),
         ];
         echo view('Modules\Product\Views\list', $data);
        }
     } else echo view('errors\html\error_404'); 
  }

  public function data(){
      $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
      $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
      $this->edit = $this->ionAuth->permissionAction($menu->id,'edit',$groupId);
      $this->delete = $this->ionAuth->permissionAction($menu->id,'delete',$groupId);

      $where = "1=1";
      $request = esc($this->request->getPost());
      $name_product = $request['name_product'];
      $category = $request['category'];
      $image = $request['image'];
      $hilight = $request['hilight'];
      $position = $request['position'];
      
      $cols = array();
      if (!empty($name_product)) { $cols['name_product'] = $name_product; }
      if (!empty($category)) { $cols['category'] = $category; }
      if (!empty($image)) { $cols['image'] = $image; }
      if (!empty($hilight)) { $cols['hilight'] = $hilight; }
      if (!empty($position)) { $cols['position'] = $position; }
      
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->ProductModel->filter($cols, $search, $limit, $start, $orderFields, $orderDir,$where)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '';
            
            $btn_actions .= '
                           <div class="dropdown dropdown-inline mr-4 dropright">
                              <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="ki ki-bold-more-hor"></i>
                              </button>
                              <div class="dropdown-menu">
                                 <ul class="navi navi-hover">';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-edit" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-pencil-alt text-warning"></i>
                                          </span>
                                          <span class="navi-text">Edit</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-delete" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-trash text-danger"></i>
                                          </span>
                                          <span class="navi-text">Delete</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                 </ul>
                              </div>
                           </div>';
            $category = $this->CategoryProductModel->select('category,link')->find($r->category);
            if($r->hilight==1) $hilight='Ya'; else $hilight='Tidak'; 
            $image='<a target="_blank" href="'.base_url("getfile?path=product/".$category['link']."&image=" . $r->image).'">'.$r->image.'</a>';
            
            $row[] = $btn_actions;
            $row[] = $no;
            $row[] = $r->name_product;
            $row[] = $category['category'];
            $row[] = $image;
            $row[] = $hilight;
            $row[] = $r->position;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->ProductModel->countTotal($where),
         'recordsFiltered' => $this->ProductModel->countFilter($cols, $search, $where),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
      'category' => $this->CategoryProductModel->findAll(),
   ];
   return view('Modules\Product\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $category=$this->CategoryProductModel->select('id')->where('link',$this->request->getPost('category'))->get()->getRow();
         $data = [
               'name_product' => $this->request->getPost('name_product'),
               'category' => $category->id,
               'image' => str_replace(str_split('\\/:*?"<>|- '), '', $this->request->getPost('AttachedDocument')),
               'hilight' => $this->request->getPost('hilight'),
               'position' => $this->request->getPost('position'),
              'user_created' => $this->session->get('user_id'),
         ];

         if ($this->ProductModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->ProductModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id'); 
      $main=$this->ProductModel->find($id);     
      $data = [
         'category' => $this->CategoryProductModel->findAll(),
         'main' => $this->ProductModel->find($id),
         'linkcategory' => $this->CategoryProductModel->find($main['category']),
      ];
      return view('Modules\Product\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $category=$this->CategoryProductModel->select('id')->where('link',$this->request->getPost('category'))->get()->getRow();
         $data = [   
               'name_product' => $this->request->getPost('name_product'),
               'category' => $category->id,
               'image' => str_replace(str_split('\\/:*?"<>|- '), '', $this->request->getPost('AttachedDocument')),
               'hilight' => $this->request->getPost('hilight'),
               'position' => $this->request->getPost('position'),
              'user_update' => $this->session->get('user_id'),
         ];
         if ($this->ProductModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->ProductModel->errors());
      }
    }

    public function delete($id)
    {
        if ($found = $this->ProductModel->where('id', $id)->delete()) {
            return $this->respondDeleted($found);
        }
        return $this->fail('Fail deleted');
    }

    public function import()
    {
      $file = $this->request->getFile('file');
      $path = $this->request->getPost('path');
      $fileName = $file->getRandomName();
      // /public/theme/frontend/img/product/cuttingboardpvc
      if (($file->move(WRITEPATH . "/uploads/product/".$path, $fileName))) {      
         echo json_encode($fileName);
      }
    }

    private function rules(){
      $this->validation->setRules([
         'name_product' => [
            'label' => 'Name Product',
            'rules' => 'required|string'
         ],
         'category' => [
            'label' => 'Category',
            'rules' => 'required|string'
         ],
         'AttachedDocument' => [
            'label' => 'AttachedDocument',
            'rules' => 'required|string'
         ],
         'hilight' => [
            'label' => 'Hilight',
            'rules' => 'required|numeric|max_length[1]'
         ],
         'position' => [
            'label' => 'Position',
            'rules' => 'required|numeric|max_length[11]'
         ],
      ]);
   }

}
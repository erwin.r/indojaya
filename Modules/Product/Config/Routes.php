<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('product', ['namespace' => 'Modules\Product\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'Product::index');
          $routes->get('edit', 'Product::load_form_edit');
          $routes->get('add', 'Product::load_form_add');
          $routes->post('updated', 'Product::updated');
          $routes->post('addnew', 'Product::new');
          $routes->post('data', 'Product::data');
          $routes->post('import', 'Product::import');
          $routes->resource('resources', [
              'controller' => 'Product',
          ]);
      });
      
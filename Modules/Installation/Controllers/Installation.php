<?php
namespace Modules\Installation\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Installation\Models\InstallationModel;
class Installation extends BaseController{
  use ResponseTrait;

  protected $InstallationModel;
  protected $edit;
  protected $delete;

  public function __construct() {
      $this->validation = \Config\Services::validation();
      $this->InstallationModel = new InstallationModel;
      $this->session = session();
      $this->edit = 0;
      $this->delete = 0;

  }

  function index(){
     $data = [
        'title' => 'Data Installation',
        'host' => site_url('installation/')
     ];
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     if($menu){
        $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
        if(!$this->ionAuth->hasAccess($menu->id,$groupId))
         echo view('errors\html\error_404'); 
        else{
         $data = [
           'title' => 'Data Installation',
           'host' => site_url('installation/'),
           'add' => $this->ionAuth->permissionAction($menu->id,'add',$groupId),
         ];
         echo view('Modules\Installation\Views\list', $data);
        }
     } else echo view('errors\html\error_404'); 
  }

  public function data(){
      $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
      $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
      $this->edit = $this->ionAuth->permissionAction($menu->id,'edit',$groupId);
      $this->delete = $this->ionAuth->permissionAction($menu->id,'delete',$groupId);

      $where = "1=1";
      $request = esc($this->request->getPost());
      $image = $request['image'];
      $description = $request['description'];
      $position = $request['position'];
      
      $cols = array();
      if (!empty($image)) { $cols['image'] = $image; }
      if (!empty($description)) { $cols['description'] = $description; }
      if (!empty($position)) { $cols['position'] = $position; }
      
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->InstallationModel->filter($cols, $search, $limit, $start, $orderFields, $orderDir,$where)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '';
            
            $btn_actions .= '
                           <div class="dropdown dropdown-inline mr-4 dropright">
                              <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="ki ki-bold-more-hor"></i>
                              </button>
                              <div class="dropdown-menu">
                                 <ul class="navi navi-hover">';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-edit" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-pencil-alt text-warning"></i>
                                          </span>
                                          <span class="navi-text">Edit</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-delete" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-trash text-danger"></i>
                                          </span>
                                          <span class="navi-text">Delete</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                 </ul>
                              </div>
                           </div>';

            $image='<a target="_blank" href="'.base_url("getfile?path=installation&image=" . $r->image).'">'.$r->image.'</a>';
            
            $row[] = $btn_actions;
            $row[] = $no;
            $row[] = $image;
            $row[] = $r->description;
            $row[] = $r->position;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->InstallationModel->countTotal($where),
         'recordsFiltered' => $this->InstallationModel->countFilter($cols, $search, $where),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
   ];
   return view('Modules\Installation\Views\add', $data);
  }

  public function import()
   {
   $file = $this->request->getFile('file');
   $fileName = $file->getRandomName();
   if (($file->move(WRITEPATH . "/uploads/installation", $fileName))) {      
      echo json_encode($fileName);
   }
   }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [               
               'image' => str_replace(str_split('\\/:*?"<>|- '), '', $this->request->getPost('AttachedDocument')),
               'description' => $this->request->getPost('description'),
               'position' => $this->request->getPost('position'),
              'user_created' => $this->session->get('user_id'),
         ];

         if ($this->InstallationModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->InstallationModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
      ];
      $data['main'] = $this->InstallationModel->find($id);
      return view('Modules\Installation\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
               'image' => str_replace(str_split('\\/:*?"<>|- '), '', $this->request->getPost('AttachedDocument')),              
               'description' => $this->request->getPost('description'),
               'position' => $this->request->getPost('position'),
              'user_update' => $this->session->get('user_id'),
         ];
         if ($this->InstallationModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->InstallationModel->errors());
      }
    }

    public function delete($id)
    {
        if ($found = $this->InstallationModel->where('id', $id)->delete()) {
            return $this->respondDeleted($found);
        }
        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'AttachedDocument' => [
            'label' => 'Image',
            'rules' => 'required|string'
         ],
         'description' => [
            'label' => 'Description',
            'rules' => 'required|string'
         ],
         'position' => [
            'label' => 'Position',
            'rules' => 'required|numeric|max_length[11]'
         ],
      ]);
   }

}
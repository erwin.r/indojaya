<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('installation', ['namespace' => 'Modules\Installation\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'Installation::index');
          $routes->get('edit', 'Installation::load_form_edit');
          $routes->get('add', 'Installation::load_form_add');
          $routes->post('updated', 'Installation::updated');
          $routes->post('addnew', 'Installation::new');
          $routes->post('data', 'Installation::data');
          $routes->post('import', 'Installation::import');
          $routes->resource('resources', [
              'controller' => 'Installation',
          ]);
      });
      
<form id="form-edit" accept-charset="utf-8">
    <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">   
   <div class="row">
      <div class="col-md-8">
         <div class="form-group">
            <label for="description">Description</label>
            <textarea type="text" name="description" class="form-control" ><?= !empty($main['description']) ? $main['description'] : '' ?></textarea>
            <div class="invalid-feedback description"></div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="position">Position</label>
            <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
            <div class="invalid-feedback position"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="AttachedDocument">Attached Document</label>
            <div class="dropzone dropzone-default dz-clickable" id="kt_dropzone_1">
               <div class="dropzone-msg dz-message needsclick">
                  <h3 class="dropzone-msg-title">Drop files here or click to load</h3>
               </div>
            </div>
            <input type="hidden" value="<?= !empty($main['image']) ? $main['image'] : '' ?>" name="AttachedDocument" class="form-control AttachedDocument" id="AttachedDocument" />
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
<?php
namespace Modules\Installation\Models;
use CodeIgniter\Model;

class InstallationModel extends Model{
   protected $table      = 'installation';
   protected $primaryKey = 'id';
   // if use uuid set to false
   protected $useAutoIncrement = true;
   protected $useSoftDeletes = true;
   protected $useTimestamps = true;
   protected $createdField  = 'created_at';
   protected $updatedField  = 'updated_at';
   protected $deletedField  = 'deleted_at';
   protected $allowedFields = ['image', 'description', 'position', 'created_at', 'user_created', 'updated_at', 'user_update', 'deleted_at'];
   protected $searchFields = ['image', 'description', 'position', 'created_at', 'user_created'];

   public function filter($cols = null, $search = null, $limit = null, $start = null, $orderField = null, $orderDir = null, $where = null){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if(!empty($cols)){
         foreach ($cols as $col => $value)
         {
            $builder->like($col, $value);
         }
      }

      // Secara bawaan menampilkan data sebanyak kurang dari
      // atau sama dengan 7 kolom pertama.
      $builder->select('installation.id, installation.image, installation.description, installation.position, installation.created_at, installation.user_created, installation.updated_at')
              ->where('installation.deleted_at is NULL')
              ->where($where)
              ->orderBy('installation.created_at', 'DESC')
              ->orderBy($orderField, $orderDir)
              ->limit($limit, $start);

      $query = $builder;

      return $query;
   }

   public function countTotal($where){
      return $this->table($this->table)->where($where)
                  ->countAllResults();
   }

   public function countFilter($cols, $search, $where){
      $builder = $this->table($this->table)->where($where);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if (!empty($cols)) {
        foreach ($cols as $col => $value) {
          $builder->like($col, $value);
        }
      }

      return $builder->countAllResults();
   }

}
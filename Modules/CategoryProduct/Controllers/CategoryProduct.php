<?php
namespace Modules\CategoryProduct\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\CategoryProduct\Models\CategoryProductModel;
class CategoryProduct extends BaseController{
  use ResponseTrait;

  protected $CategoryProductModel;
  protected $edit;
  protected $delete;

  public function __construct() {
      $this->validation = \Config\Services::validation();
      $this->CategoryProductModel = new CategoryProductModel;
      $this->session = session();
      $this->edit = 0;
      $this->delete = 0;

  }

  function index(){
     $data = [
        'title' => 'Data Category Product',
        'host' => site_url('categoryproduct/')
     ];
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     if($menu){
        $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
        if(!$this->ionAuth->hasAccess($menu->id,$groupId))
         echo view('errors\html\error_404'); 
        else{
         $data = [
           'title' => 'Data Category Product',
           'host' => site_url('categoryproduct/'),
           'add' => $this->ionAuth->permissionAction($menu->id,'add',$groupId),
         ];
         echo view('Modules\CategoryProduct\Views\list', $data);
        }
     } else echo view('errors\html\error_404'); 
  }

  public function data(){
      $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
      $groupId = ($this->ionAuth->getUsersGroups()->getRow()) ? $this->ionAuth->getUsersGroups()->getRow()->id : 0;
      $this->edit = $this->ionAuth->permissionAction($menu->id,'edit',$groupId);
      $this->delete = $this->ionAuth->permissionAction($menu->id,'delete',$groupId);

      $where = "1=1";
      $request = esc($this->request->getPost());
      $category = $request['category'];
      $link = $request['link'];
      $keyword = $request['keyword'];
      $hilight = $request['hilight'];
      $hilight_jasa = $request['hilight_jasa'];
      $position = $request['position'];
      
      $cols = array();
      if (!empty($category)) { $cols['category'] = $category; }
      if (!empty($link)) { $cols['link'] = $link; }
      if (!empty($keyword)) { $cols['keyword'] = $keyword; }
      if (!empty($hilight)) { $cols['hilight'] = $hilight; }
      if (!empty($hilight_jasa)) { $cols['hilight_jasa'] = $hilight_jasa; }
      if (!empty($position)) { $cols['position'] = $position; }
      
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->CategoryProductModel->filter($cols, $search, $limit, $start, $orderFields, $orderDir,$where)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '';
            
            $btn_actions .= '
                           <div class="dropdown dropdown-inline mr-4 dropright">
                              <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="ki ki-bold-more-hor"></i>
                              </button>
                              <div class="dropdown-menu">
                                 <ul class="navi navi-hover">';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-edit" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-pencil-alt text-warning"></i>
                                          </span>
                                          <span class="navi-text">Edit</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                    <li class="navi-item">
                                       <a class="navi-link btn-delete" data-id="'.$r->id.'">
                                          <span class="navi-icon">
                                             <i class="fas fa-trash text-danger"></i>
                                          </span>
                                          <span class="navi-text">Delete</span>
                                       </a>
                                    </li>';
            $btn_actions .= '
                                 </ul>
                              </div>
                           </div>';

            if($r->hilight==1) $hilight='Ya'; else $hilight='Tidak'; 
            if($r->hilight_jasa==1) $hilight_jasa='Ya'; else $hilight_jasa='Tidak'; 
            
            $row[] = $btn_actions;
            $row[] = $no;
            $row[] = $r->category;
            $row[] = $r->link;
            $row[] = $r->keyword;
            $row[] = $hilight;
            $row[] = $hilight_jasa;
            $row[] = $r->position;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->CategoryProductModel->countTotal($where),
         'recordsFiltered' => $this->CategoryProductModel->countFilter($cols, $search, $where),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
   ];
   return view('Modules\CategoryProduct\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
               'category' => $this->request->getPost('category'),
               'link' => $this->request->getPost('link'),
               'keyword' => $this->request->getPost('keyword'),
               'hilight' => $this->request->getPost('hilight'),
               'hilight_jasa' => $this->request->getPost('hilight_jasa'),
               'position' => $this->request->getPost('position'),
              'user_created' => $this->session->get('user_id'),
         ];

         if ($this->CategoryProductModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->CategoryProductModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
      ];
      $data['main'] = $this->CategoryProductModel->find($id);
      return view('Modules\CategoryProduct\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->getErrors()
         ], 400);
 
      }else{
         $data = [
   
               'category' => $this->request->getPost('category'),
               'link' => $this->request->getPost('link'),
               'keyword' => $this->request->getPost('keyword'),
               'hilight' => $this->request->getPost('hilight'),
               'hilight_jasa' => $this->request->getPost('hilight_jasa'),
               'position' => $this->request->getPost('position'),
              'user_update' => $this->session->get('user_id'),
         ];
         if ($this->CategoryProductModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->CategoryProductModel->errors());
      }
    }

    public function delete($id)
    {
        if ($found = $this->CategoryProductModel->where('id', $id)->delete()) {
            return $this->respondDeleted($found);
        }
        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'category' => [
            'label' => 'Category',
            'rules' => 'required|string|max_length[50]'
         ],
         'link' => [
            'label' => 'Link',
            'rules' => 'required|string|max_length[30]'
         ],
         'hilight' => [
            'label' => 'Hilight',
            'rules' => 'required|numeric|max_length[1]'
         ],
         'hilight_jasa' => [
            'label' => 'Hilight Jasa',
            'rules' => 'required|numeric|max_length[1]'
         ],
         'position' => [
            'label' => 'Position',
            'rules' => 'required|numeric|max_length[2]'
         ],
      ]);
   }

}
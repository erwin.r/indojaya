<form id="form-edit" accept-charset="utf-8">
    <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
   <div class="row">
      <div class="col-md-6">
         <div class="form-group">
            <label for="category">Category <span class="text-danger">*</span></label>
            <input type="text" name="category" value="<?= !empty($main['category']) ? $main['category'] : '' ?>" class="form-control" />
            <div class="invalid-feedback category"></div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="link">Link <span class="text-danger">*</span></label>
            <input type="text" name="link" value="<?= !empty($main['link']) ? $main['link'] : '' ?>" class="form-control" />
            <div class="invalid-feedback link"></div>
         </div>
      </div>
   </div>   
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="hilight">Hilight <span class="text-danger">*</span></label>
            <select name="hilight" class="form-control custom-select">
               <option <?= !empty($main['hilight']) && $main['hilight'] == 1 ? 'selected' : '' ?> value="1">Ya</option>
               <option <?= !empty($main['hilight']) && $main['hilight'] == 0 ? 'selected' : '' ?> value="0">Tidak</option>
            </select>             
            <div class="invalid-feedback hilight"></div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="hilight_jasa">Hilight Jasa <span class="text-danger">*</span></label>
            <select name="hilight_jasa" class="form-control custom-select">
               <option <?= !empty($main['hilight']) && $main['hilight'] == 1 ? 'selected' : '' ?> value="1">Ya</option>
               <option <?= !empty($main['hilight']) && $main['hilight'] == 0 ? 'selected' : '' ?> value="0">Tidak</option>
            </select>  
            <div class="invalid-feedback hilight_jasa"></div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="position">Position <span class="text-danger">*</span></label>
            <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
            <div class="invalid-feedback position"></div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="form-group">
            <label for="keyword">Keyword</label>
            <textarea type="text" name="keyword" class="form-control" ><?= !empty($main['keyword']) ? $main['keyword'] : '' ?></textarea>
            <div class="invalid-feedback keyword"></div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>
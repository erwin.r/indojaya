<?php
namespace Modules\CategoryProduct\Models;
use CodeIgniter\Model;

class CategoryProductModel extends Model{
   protected $table      = 'category_product';
   protected $primaryKey = 'id';
   // if use uuid set to false
   protected $useAutoIncrement = true;
   protected $useSoftDeletes = true;
   protected $useTimestamps = true;
   protected $createdField  = 'created_at';
   protected $updatedField  = 'updated_at';
   protected $deletedField  = 'deleted_at';
   protected $allowedFields = ['category', 'link', 'keyword', 'hilight', 'hilight_jasa', 'position', 'created_at', 'user_created', 'updated_at', 'user_update', 'deleted_at'];
   protected $searchFields = ['category', 'link', 'keyword', 'hilight', 'hilight_jasa'];

   public function filter($cols = null, $search = null, $limit = null, $start = null, $orderField = null, $orderDir = null, $where = null){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if(!empty($cols)){
         foreach ($cols as $col => $value)
         {
            $builder->like($col, $value);
         }
      }

      // Secara bawaan menampilkan data sebanyak kurang dari
      // atau sama dengan 7 kolom pertama.
      $builder->select('category_product.id, category_product.category, category_product.link, category_product.keyword, category_product.hilight, category_product.hilight_jasa, category_product.position')
              ->where('category_product.deleted_at is NULL')
              ->where($where)
              ->orderBy('category_product.created_at', 'DESC')
              ->orderBy($orderField, $orderDir)
              ->limit($limit, $start);

      $query = $builder;

      return $query;
   }

   public function countTotal($where){
      return $this->table($this->table)->where($where)
                  ->countAllResults();
   }

   public function countFilter($cols, $search, $where){
      $builder = $this->table($this->table)->where($where);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if (!empty($cols)) {
        foreach ($cols as $col => $value) {
          $builder->like($col, $value);
        }
      }

      return $builder->countAllResults();
   }

}
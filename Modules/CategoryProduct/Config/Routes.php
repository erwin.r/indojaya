<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('categoryproduct', ['namespace' => 'Modules\CategoryProduct\Controllers','filter' => 'auth'], function($routes)
      {
          $routes->get('/', 'CategoryProduct::index');
          $routes->get('edit', 'CategoryProduct::load_form_edit');
          $routes->get('add', 'CategoryProduct::load_form_add');
          $routes->post('updated', 'CategoryProduct::updated');
          $routes->post('addnew', 'CategoryProduct::new');
          $routes->post('data', 'CategoryProduct::data');
          $routes->resource('resources', [
              'controller' => 'CategoryProduct',
          ]);
      });
      
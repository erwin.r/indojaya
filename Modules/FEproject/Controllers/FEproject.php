<?php
namespace Modules\FEproject\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\FEproject\Models\FEprojectModel;
use Modules\FEdashboard\Models\FEdashboardModel;
class FEproject extends BaseController{
   use ResponseTrait;   

   public function __construct() {
   $this->validation = \Config\Services::validation();
   $this->FEprojectModel = new FEprojectModel;
   $this->FEdashboardModel = new FEdashboardModel;
   $this->db = \Config\Database::connect();

   }

   function index(){
      $data = [
        'title'   => 'Project',
        'host'    => site_url('/'),        
        'list'=> $this->FEprojectModel->getProject('1=1')->get()->getResult(),
        'category'=> $this->FEdashboardModel->getCategoryProduct('hilight_jasa=1')->get()->getResult(),
      ];
      echo view('Modules\FEproject\Views\list', $data);
   }

}
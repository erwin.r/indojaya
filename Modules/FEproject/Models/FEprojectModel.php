<?php
namespace Modules\FEproject\Models;
use CodeIgniter\Model;
use App\Controllers\BaseController;

class FEprojectModel extends Model{   
   protected $project = 'project';   
   protected $service = 'service';   

   public function getProject($where)
   {
      $builder = $this->db->table($this->project);
      $builder ->select('name_project,address,company,image')               
               ->orderBy('position','ASC')->where('deleted_at Is Null AND '.$where);
      return $builder;
   }   

   public function getJasa($where)
   {
      $builder = $this->db->table($this->service);
      $builder ->select('service.id,service.service_name,service.image,category_product.link')
               ->join('category_product','service.category=category_product.id')
               ->orderBy('service.position','ASC')->where('service.deleted_at Is Null AND '.$where);
      return $builder;
   } 
   
}
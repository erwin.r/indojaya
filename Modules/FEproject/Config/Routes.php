<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('proyek', ['namespace' => 'Modules\FEproject\Controllers'], function($routes)
      {
          $routes->get('/', 'FEproject::index');          
          $routes->resource('resources', [
              'controller' => 'FEproject',
          ]);
      });
      
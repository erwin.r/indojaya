<?= $this->include('layout/frontend/header') ?>        
    <div class="electro-product-wrapper wrapper-padding pb-45">
        <div class="container-fluid">
            <div class="section-title-4 text-center mb-40">
                <h2>Project Kami</h2>
            </div>
            <div class="top-product-style">                
                <div class="tab-content">                                        
                    <div class="row">
                        <?php foreach ($list as $r) {?>
                        <div class="col-lg-6">
                            <div class="menu-product-wrapper">
                                <div class="single-menu-product mb-30">
                                    <div class="menu-product-img">
                                        <img data-src="<?= (!empty($r->image)) ? base_url("getfile?path=project&image=" . $r->image) : "" ?>" class="lazyload" alt="">
                                    </div>
                                    <div class="menu-product-content">
                                        <h4><a href="product-details.html"><?php echo $r->name_project; ?></a></h4>
                                        <div class="menu-product-price-rating">
                                            <div class="menu-product-price">                                                
                                                <span class="menu-product-new"><?php echo $r->company; ?></a></span>
                                            </div>                                            
                                        </div>
                                        <p><?php echo $r->address; ?></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>                        
                    </div>
                </div>
            </div>
            <br>
            <div class="section-title-4 text-center mb-40">
                <h2>Jasa</h2>
            </div>
            <div class="top-product-style">
                <div class="product-tab-list3 text-center mb-80 nav product-menu-mrg" role="tablist">
                    <?php $n=1; foreach ($category as $r) {?>
                        <a class="<?php if($n==1) echo "active"; ?>" href="#<?php echo $r->link; ?>" data-bs-toggle="tab" role="tab">
                            <h4><?php echo $r->category; ?></h4>
                        </a>
                    <?php $n++; } ?>                    
                </div>
                <div class="tab-content">
                    <?php $n=1; foreach ($category as $r) { ?>
                    <div class="tab-pane <?php if($n==1) echo "active"; ?> show fade" id="<?php echo $r->link; ?>" role="tabpanel">
                        <div class="custom-row-2">
                            <?php 
                                $this->FEprojectModel = new Modules\FEproject\Models\FEprojectModel;                                    
                                $this->ionAuth = new \IonAuth\Libraries\IonAuth();                                    
                                $setting = $this->ionAuth->getSetting();
                                $service=$this->FEprojectModel->getJasa('category_product.link="'.$r->link.'"')->get()->getResult(); 
                                foreach ($service as $p){ 
                            ?>
                            <div class="custom-col-style-2 custom-col-4">
                                <div class="product-wrapper product-border mb-24">
                                    <div class="product-img-3">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=jasa&image=" . $p->image) : "" ?>" class="lazyload" alt="Image">                                        
                                    </div>
                                    <div class="product-img-3">                                        
                                        <div class="product-action-right">
                                            <a class="animate-right" href="#" data-bs-target="#Modal<?= $p->id; ?>" data-bs-toggle="modal" title="Quick View">
                                                <i class="pe-7s-look"></i>
                                            </a>
                                            <a class="animate-top" title="Pesan Sekarang" target="_blank" href="https://wa.me/?phone=<?= $setting->contac; ?>&text=<?= $p->service_name; ?>"">
                                                <i class="pe-7s-cart"></i>
                                            </a>                                            
                                        </div>
                                    </div>
                                    <div class="product-content-4 text-center">                                        
                                        <h4><?php echo $p->service_name; ?></h4>                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="modal fade" id="Modal<?= $p->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span class="pe-7s-close" aria-hidden="true"></span>
                                </button>
                                <div class="modal-dialog modal-quickview-width" role="document">
                                    <div class="modal-content">                                        
                                        <img data-src="<?= (!empty($p->image)) ? base_url("getfile?path=jasa&image=" . $p->image) : "" ?>" class="lazyload img img-responsive" alt="">                                                                                
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php $n++; } ?>                    
                </div>
            </div>
        </div>
    </div>
    <?= $this->include('layout/frontend/footer') ?>
    <!-- all js here -->
    <script src="<?= base_url() ?>/theme/frontend/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/popper.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/isotope.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/waypoints.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/ajax-mail.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/plugins.js"></script>
    <script src="<?= base_url() ?>/theme/frontend/js/main.js"></script>
</body>

</html>